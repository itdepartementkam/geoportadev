<?php include_once( 'includes/verif-lang.php' ) ?>
<!DOCTYPE html>
<html class = 'no-js css-menubar' lang = 'en'>

<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:33:04 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Dashboard</title>

  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="../global/css/bootstrap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/css/bootstrap-extend.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="assets/css/site.minfd53.css?v4.0.1">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="../global/css/skintools.minfd53.css?v4.0.1">
  <script src="assets/js/Plugin/skintools.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="../global/vendor/animsition/animsition.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/asscrollable/asScrollable.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/switchery/switchery.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/intro-js/introjs.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/slidepanel/slidePanel.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jquery-mmenu/jquery-mmenu.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/flag-icon-css/flag-icon.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/waves/waves.minfd53.css?v4.0.1">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="../global/vendor/chartist/chartist.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jvectormap/jquery-jvectormap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.css?v4.0.1">

  <!-- Page -->
  <link rel="stylesheet" href="assets/examples/css/dashboard/v1.minfd53.css?v4.0.1">

  <!-- Fonts -->
  <link rel="stylesheet" href="../global/fonts/material-design/material-design.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/fonts/brand-icons/brand-icons.minfd53.css?v4.0.1">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">


  <!-- Scripts -->
  <script src="../global/vendor/breakpoints/breakpoints.minfd53.js?v4.0.1"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="animsition site-navbar-small dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


<?php
 require('../../models/Warehousemaneger.php');
 require('../../models/city.php');
 require('../../models/Language.php');
 require('../../models/Warehouse.php');
$WH=new Warehousemaneger();
?>
 <?php include 'includes/navbar.php';?>
 <?php include 'includes/menu.php';?>


  <!-- Page -->
<div class="page">
 <!-- Panel Full Example -->
 <div class="page-content container-fluid">
 <div class="col-md-12">
          <!-- Panel Static Labels -->
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Update Responsable</h3>
            </div>
          </div>
          <div class="panel-body container-fluid">
              <form method="POST">
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="FirstName">First Name</label>
                  <input type="text" class="form-control" id="FirstNameid" name="FirstName" placeholder=""
                  />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="LastName">Last Name</label>
                  <input type="text" class="form-control" id="LastNameid" name="LastName" placeholder=""/>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="address">address</label>
                  <input type="text" class="form-control" id="addressid" name="address" placeholder=""
                  />
                </div>

                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="user">Email</label>
                  <input type="Email" class="form-control" id="userid" name="email" placeholder=""
                  />
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="phone1">Phone 1</label>
                  <input type="tel" class="form-control" id="phone1" name="phone1" placeholder="+49 12 345 678"
                  />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="Phone2">Phone 2</label>
                  <input type="tel" class="form-control" id="Phone2" name="Phone2"
                    placeholder="+49 12 345 678" />
                </div>

                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="select">Language</label>

                  <select name="language" class="form-control" class="col-sm-30" id="city">
              <?php  $Language=new Language();
        foreach ($Language->getAll() as $lan)
        {
           $langu=$lan->getId();
                echo "<option value='$langu'>".$lan->getName()."</option>";

       }      ?>

        </select>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="selectMulti">City</label>
                  <select name="city" class="form-control" class="col-sm-30" id="city">
    <?php  $city=new City();
        foreach ($city->getAll() as $c)
        {
           $cities=$c->getId();
                echo "<option value='$cities'>".$c->getName()."</option>";

       }      ?>

       </select>
      </div>
                  <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="user">Password</label>
                  <input type="Password" class="form-control" id="Passwordt" name="Passwordt" placeholder=""
                  />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="user">confirm your Password</label>
                  <input type="Password" class="form-control" id="Passwordd" name="Passwordd" placeholder=""
                  />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="Warehouse">Warehouse</label>

                  <select name="warehouse" class="form-control" class="col-sm-30" id="warehouse">
    <?php  $Warehouse=new Warehouse();
        foreach ($Warehouse->getAll() as $w)
        {
           $Ware=$w->getId();
                echo "<option value='$Ware'>".$w->getName()."</option>";

       }      ?>

        </select>
      </div>

      <button type="submit" name="btn" class="btn btn-primary mb-2" >update</button>
      <button type="reset"name="btn" class="btn btn-secondary mb-2">cancel</button>



              </form>

        </div>
          <!-- End Panel Static Labels -->
      </div>
    </div>
  </div>
  <!-- End Page -->
<?php
if ( isset( $_POST['btn'] ) ) {

$FirstName = $_POST['FirstName'];
$LastName = $_POST['LastName'];
$phone1 = $_POST['phone1'];
$phone2 = $_POST['phone2'];
$language = $_POST['language'];
$email = $_POST['email'];
$city = $_POST['city'];
$Passwordt = $_POST['Passwordt'];
$Passwordd = $_POST['Passwordd'];
$warehouse = $_POST['warehouse'];
$WH->setFirstName( $FirstName );
$WH->setLastName( $LastName );
$WH->setPhon1( $phone1 );
$WH->setIdLanguge( $language );
$WH->setPhon1( $phone2 );
$WHn->setHeight( $email );
$WH->setCity()->setId( $city );
$WH->setUser()->setPassword( $Passwordt );
$WH->setUser()->setPassword( $Passwordd );
$WH->setWarehouse( $warehouse );

$dimension->update( ( int )$dimension->getId() );

}
?>

  <!-- Footer -->
  <?php include 'includes/footer.php';?>
  <!-- Core  -->
  <script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="../global/vendor/babel-external-helpers/babel-external-helpersfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jquery/jquery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/popper-js/umd/popper.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/bootstrap/bootstrap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/animsition/animsition.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/mousewheel/jquery.mousewheel.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollbar/jquery-asScrollbar.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollable/jquery-asScrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/waves/waves.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <script src="../global/vendor/jquery-mmenu/jquery.mmenu.min.allfd53.js?v4.0.1"></script>
  <script src="../global/vendor/switchery/switchery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/intro-js/intro.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/screenfull/screenfull.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/slidepanel/jquery-slidePanel.minfd53.js?v4.0.1"></script>

  <!-- Plugins For This Page -->
  <script src="../global/vendor/chartist/chartist.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/jquery-jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-enfd53.js?v4.0.1"></script>
  <script src="../global/vendor/matchheight/jquery.matchHeight-minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/peity/jquery.peity.minfd53.js?v4.0.1"></script>

  <!-- Scripts -->
  <script src="../global/js/Component.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Base.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Config.minfd53.js?v4.0.1"></script>

  <script src="assets/js/Section/Menubar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/Sidebar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/PageAside.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/GridMenu.minfd53.js?v4.0.1"></script>
  <!-- Config -->
  <script src="../global/js/config/colors.minfd53.js?v4.0.1"></script>
  <script src="assets/js/config/tour.minfd53.js?v4.0.1"></script>
  <script>
    Config.set('assets', 'assets');
  </script>

  <!-- Page -->
  <script src="assets/js/Site.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/asscrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/slidepanel.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/switchery.minfd53.js?v4.0.1"></script>

  <script src="../global/js/Plugin/matchheight.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/peity.minfd53.js?v4.0.1"></script>


  <script src="assets/examples/js/dashboard/v1.minfd53.js?v4.0.1"></script>


</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:34:21 GMT -->
</html>