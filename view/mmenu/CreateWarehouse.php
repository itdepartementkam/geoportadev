<?php
require('../../models/Warehouse.php');
require('../../models/Warehousemaneger.php');
require('../../models/City.php');
session_start();
$warehouse=new Warehouse();
$warehouseName=$_POST['name'];
$warehouseAdresse=$_POST['Adresse'];
$warehouse->setName($warehouseName);
$warehouse->setAddress($warehouseAdresse);

$warehouseManagerName=$_POST['manager'];
$warehouseManager=new Warehousemaneger();
$warehouseManager->setFirstname($warehouseManagerName);
$warehouse->setIdWarehousemanger($warehouseManager->getId());

$cityname=$_POST['city'];
$city=new City();
$city->setName($cityname);
$city->setWarehouseId($warehouse->getId());
$warehouse->create();

if (!(is_null($warehouse))) {
    echo json_encode(array("statusCode"=>200));
} 
else {
    echo json_encode(array("statusCode"=>201));
}



?>