<?php
 include_once("includes/verif-lang.php");
 require('../../models/Customer.php');
 require('../../models/Language.php');
 require('../../models/City.php');
 require('../../models/Country.php');

 include 'includes/navbar.php';
 include 'includes/menu.php';
 $Customer=new Customer(); 
 $language=new Language();
 $cities=new City();
 $country=new Country();
 ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:33:04 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Dashboard</title>

  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="../global/css/bootstrap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/css/bootstrap-extend.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="assets/css/site.minfd53.css?v4.0.1">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="../global/css/skintools.minfd53.css?v4.0.1">
  <script src="assets/js/Plugin/skintools.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="../global/vendor/animsition/animsition.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/asscrollable/asScrollable.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/switchery/switchery.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/intro-js/introjs.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/slidepanel/slidePanel.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jquery-mmenu/jquery-mmenu.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/flag-icon-css/flag-icon.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/waves/waves.minfd53.css?v4.0.1">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="../global/vendor/chartist/chartist.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jvectormap/jquery-jvectormap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.css?v4.0.1">

  <!-- Page -->
  <link rel="stylesheet" href="assets/examples/css/dashboard/v1.minfd53.css?v4.0.1">

  <!-- Fonts -->
  <link rel="stylesheet" href="../global/fonts/material-design/material-design.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/fonts/brand-icons/brand-icons.minfd53.css?v4.0.1">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">


  <!-- Scripts -->
  <script src="../global/vendor/breakpoints/breakpoints.minfd53.js?v4.0.1"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="animsition site-navbar-small dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  

  <!-- Page -->
  <div class="page">

        <!-- Panel Table Add Row -->
        <div class="panel Gh-Warehouse">
        <div class="container">
        <div class="row">
        <div class="col-lg-10 bg-white vh-100">
        <div class="panel ">
          <header class="panel-heading">
          <h3 class="panel-title"><?php echo ListCustomers; ?></h3>
          </header>
          <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <div class="mb-15">
                <button id="addToTable" class="btn btn-primary" type="button" data-toggle="modal" data-target="#editor-modal">
                  <i class="icon md-plus" aria-hidden="true"></i> <?php echo AddCustomer; ?>
                </button>
              </div>
            </div>
          </div>
          <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
            <thead>
              <tr>
                <th><?php echo FirstName; ?></th>
                <th><?php echo LastName; ?></th>
                <th><?php echo Address; ?></th>
                <th><?php echo Phone1; ?></th>
                <th><?php echo Actions; ?></th>
              </tr>
            </thead>
            <tbody>
              
            <?php foreach ($Customer->getAll() as $c) {  ?>
            <tr>
            <td><?= $c->getFirstname(); ?></td>
            <td><?= $c->getLastname(); ?></td>
            <td><?= $c->getAddress(); ?></td>
            <td><?= $c->getPhone1(); ?></td>
            <?php $cli=$c->getId() ?> 
            <td>
            <a href="updatecustomer.php?lang=fr&id=<?= $cli ?>" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                    data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i><?php echo update; ?></a></td>
             </tr>
              <?php } ?>  

            </tbody>
          </table>
         </div>
        </div>
        </div>
        </div>
        </div>
        
        </div>
         
      </div>
      <!-- End Panel Table Add Row -->

      <? ////////////////modal for create new customer///////////////// ?>
          <div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-simple" role="document">
              <form class="modal-content form-horizontal" id="editor">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="editor-title">Ajouter un client</h4>
                </div>

                <div class="modal-body">
                  <input type="number" id="id" name="id" class="hidden" style="display:none;">
                  <div class="form-group required">
                    <label for="Name" class="col-sm-3 control-label ">firstname</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="firstname" required="">
                    </div>
                  </div>

                  <div class="form-group required">
                    <label for="Name" class="col-sm-3 control-label ">lastname</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="lastname" required="">
                    </div>
                  </div>

                  <div class="form-group required">
                    <label for="Adresse" class="col-sm-3 control-label">Adresse</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="Adresse" name="Adresse" placeholder="Adresse" required="">
                    </div>
                  </div>

                  <div class="form-group required">
                    <label for="phone1" class="col-sm-3 control-label">phone1</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="phone1" name="phone1" placeholder="phone1" required="">
                    </div>
                  </div>
                  
                  <div class="form-group required">
                    <label for="phone2" class="col-sm-3 control-label">phone2</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="phone2" name="phone2" placeholder="phone2" required="">
                    </div>
                  </div>

                  <div class="form-group required">
                    <label for="Email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                      <input type="email" class="form-control" id="Email" name="Email" placeholder="Email" required="">
                    </div>
                  </div>

                  <div class="form-group required">
                    <label for="password" class="col-sm-3 control-label">Password:</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" id="password" name="password" placeholder="password" required="">
                    </div>
                  </div>
                  
                  <div class="form-group required">
                  <label for="language" class="col-sm-3 control-label" >Language</label>
                  <div class="col-sm-9">
                    <select class="custom-select" id="inputGroupSelect01" name="language" required>
                    <option value="" selected>choose...</option>
                    <?php foreach ($language->getAll() as $b) {?>
                    <option value="<?php echo $b->getId(); ?>"><?php echo $b->getName(); ?></option>
                    <?php } ?>
                    </select>
                  </div>
                  </div>

                  <div class="form-group required">
                  <label for="city" class="col-sm-3 control-label" >Cities</label>
                  <div class="col-sm-9">
                    <select class="custom-select" id="inputGroupSelect02" name="city" required>
                    <option value="" selected>choose...</option>
                    <?php foreach ($cities->getAll() as $c) {?>
                    <option value="<?php echo $c->getId(); ?>"><?php echo $c->getName(); ?></option>
                    <?php } ?>
                    </select>
                  </div>
                  </div>

                  <div class="form-group required">
                  <label for="country" class="col-sm-3 control-label" >Country</label>
                  <div class="col-sm-9">
                    <select class="custom-select" id="inputGroupSelect03" name="country" required>
                    <option value="" selected>choose...</option>
                    <?php foreach ($country->getAll() as $c) {?>
                    <option value="<?php echo $c->getId(); ?>"><?php echo $c->getName(); ?></option>
                    <?php } ?>
                    </select>
                  </div>
                  </div>

                 </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary waves-effect waves-light waves-round" id="submitSave">save</button>
                  <button type="button" class="btn btn-default waves-effect waves-light waves-round" data-dismiss="modal">cancel</button>
                </div>
              </form>
            </div>
          </div>

          <? ////////////////end modal for create///////////////// ?>
          </div>
          </div>
        </div>
  </div>
  </div>
 </div>
  <!-- End Page -->


  <!-- Footer -->
  <?php include 'includes/footer.php';?>
  <!-- Core  -->
  <script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="../global/vendor/babel-external-helpers/babel-external-helpersfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jquery/jquery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/popper-js/umd/popper.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/bootstrap/bootstrap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/animsition/animsition.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/mousewheel/jquery.mousewheel.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollbar/jquery-asScrollbar.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollable/jquery-asScrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/waves/waves.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <script src="../global/vendor/jquery-mmenu/jquery.mmenu.min.allfd53.js?v4.0.1"></script>
  <script src="../global/vendor/switchery/switchery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/intro-js/intro.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/screenfull/screenfull.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/slidepanel/jquery-slidePanel.minfd53.js?v4.0.1"></script>

  <!-- Plugins For This Page -->
  <script src="../global/vendor/chartist/chartist.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/jquery-jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-enfd53.js?v4.0.1"></script>
  <script src="../global/vendor/matchheight/jquery.matchHeight-minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/peity/jquery.peity.minfd53.js?v4.0.1"></script>

  <!-- Scripts -->
  <script src="../global/js/Component.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Base.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Config.minfd53.js?v4.0.1"></script>

  <script src="assets/js/Section/Menubar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/Sidebar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/PageAside.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/GridMenu.minfd53.js?v4.0.1"></script>
  <!-- Config -->
  <script src="../global/js/config/colors.minfd53.js?v4.0.1"></script>
  <script src="assets/js/config/tour.minfd53.js?v4.0.1"></script>
  <script>
    Config.set('assets', 'assets');
  </script>

  <!-- Page -->
  <script src="assets/js/Site.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/asscrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/slidepanel.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/switchery.minfd53.js?v4.0.1"></script>

  <script src="../global/js/Plugin/matchheight.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/peity.minfd53.js?v4.0.1"></script>


  <script src="assets/examples/js/dashboard/v1.minfd53.js?v4.0.1"></script>


</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
 
    $("#submitSave").click(function(e){
        e.preventDefault();
 
        $.post(
            'http://localhost/geoportaDev/view/mmenu/AddCustomer.php', 
            {
                firstname : $("#firstname").val(),
                lastname : $("#lastname").val(),
                Adresse: $("#Adresse").val(),
                phone1: $("#phone1").val(),
                phone2: $("#phone1").val(),
                language: $("#inputGroupSelect01").val(),
                Email: $('#Email').val(),
                password: $('#password').val(),
                city: $('#inputGroupSelect02').val(),
                country: $('#inputGroupSelect03').val()
            },
 
            function(dataResult){
              try{
              var dataResult = JSON.parse(dataResult);
                if(dataResult.statusCode==200){
                    alert('data added');
                    window.location.href="http://localhost/geoportaDev/view/mmenu/client.php";
                }
                else{
                    alert('failed');
                }
         
            }
            catch (e) {
              console.error(e)
              console.error('JSON recived :', dataResult)
                }
            
            }
            ,
            'text'
           
         );
    });
});
</script>
<?php ////// END CALL AJAX FOR CREATE NEW Customer /////////////// ?>
<?php ////////////////////////////////////////////////////////////// ?>
