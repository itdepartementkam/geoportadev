<?php
  include_once("includes/verif-lang.php");
  require('../../models/Warehouse.php');
  require('../../models/Warehousemaneger.php');
  require('../../models/City.php');
  $warehouse=new Warehouse();
  // new add
  $warehouseManager=new Warehousemaneger();
  $cities=new City();?>
 <?php include 'includes/navbar.php';?>
 <?php include 'includes/menu.php';?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:33:04 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title><?php echo title_warehouse; ?> </title>

  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="../global/css/bootstrap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/css/bootstrap-extend.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="assets/css/site.minfd53.css?v4.0.1">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="../global/css/skintools.minfd53.css?v4.0.1">
  <script src="assets/js/Plugin/skintools.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="../global/vendor/animsition/animsition.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/asscrollable/asScrollable.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/switchery/switchery.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/intro-js/introjs.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/slidepanel/slidePanel.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jquery-mmenu/jquery-mmenu.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/flag-icon-css/flag-icon.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/waves/waves.minfd53.css?v4.0.1">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="../global/vendor/chartist/chartist.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jvectormap/jquery-jvectormap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.css?v4.0.1">

  <!-- Page -->
  <link rel="stylesheet" href="assets/examples/css/dashboard/v1.minfd53.css?v4.0.1">

  <!-- Fonts -->
  <link rel="stylesheet" href="../global/fonts/material-design/material-design.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/fonts/brand-icons/brand-icons.minfd53.css?v4.0.1">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">


  <!-- Scripts -->
  <script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.js"></script>
  <script src="../global/vendor/breakpoints/breakpoints.minfd53.js?v4.0.1"></script>
  <script>
    Breakpoints();
  </script>
</head>

<body class="animsition site-navbar-small dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
 
  <!-- Page -->
  <div class="page Gh-Warehouse">
    <div class="container">
        <div class="row">
        <div class="col-lg-10 bg-white vh-100">
        <div class="panel ">
            <header class="panel-heading">
              <h3 class="panel-title"><?php echo show_warehouse; ?></h3>
            </header>
            <div class="panel-body">
              <table id="exampleFooEditing" class="table table-bordered table-hover toggle-circle footable footable-10 footable-editing footable-editing-right footable-editing-no-view footable-filtering footable-filtering-right footable-paging footable-paging-center breakpoint breakpoint-md" data-paging="true" data-filtering="true" data-sorting="true" style="">
                <thead>
                <tr class="footable-filtering">
                <th colspan="6">
                <form class="form-inline">
                <div class="form-group footable-filtering-search">
                <label class="sr-only"><?php echo Search; ?></label>
                <div class="input-group">
                <input type="text" class="form-control" placeholder="Search">
                <div class="input-group-btn">
                <button type="button" class="btn btn-primary">
                <span class="fooicon icon md-search"></span>
                </button>
                <button type="button" class="btn btn-default dropdown-toggle">
                <span class="caret"></span>
                </button>
                </div>
                </div>
                </div>
                </form>
                </th>
                </tr>

                <tr class="footable-header">
                  <th data-name="name" class="footable-sortable" style="display: table-cell;">
                  <?php echo name_warehouse; ?><span class="fooicon fooicon-sort"></span>
                  </th>

                  <th data-name="name" class="footable-sortable" style="display: table-cell;">
                  <?php echo adresse; ?>
                  <span class="fooicon fooicon-sort"></span>
                 </th>

                 <th data-name="actions" class="footable-sortable" style="display: table-cell;">
                 <?php echo Actions; ?>
                 <span class="fooicon fooicon-sort"></span>
                 </th>             

                
                 
                 
                 </thead>
                <tbody>

                <?php  foreach ($warehouse->getAll() as $c){?>            
                <tr>
                 
                 <td style="display: table-cell;"><?php  echo $c->getName(); ?></td>
                 <td style="display: table-cell;"><?php echo $c->getAddress(); ?></td>
                 <td style="display: table-cell;" class="footable-last-visible">
                 <a href="UpdateWarehouse.php?lang=fr&edit_id=<?php echo $c->getId();?>">
                 <button type="button" class="btn btn-primary footable-add" ><?php echo Edit_warehouse; ?></button></a> 
                 </td>
                
                 <td class="footable-editing" style="display: none;">
                 <div class="btn-group btn-group-xs" role="group">
                 <button type="button" class="btn btn-default footable-edit">
                 <span class="fooicon fooicon-pencil" aria-hidden="true">
                 </span>
                 </button> 
                <button type="button" class="btn btn-default footable-delete">
                <span class="fooicon fooicon-trash" aria-hidden="true"></span>
                </button>
                </div>
                </td>
                </tr>
                 <?php }?>
                <tr>
                <td class="footable-editing" style="display: none;">
                <div class="btn-group btn-group-xs" role="group">
                <button type="button" class="btn btn-default footable-edit">
                <span class="fooicon fooicon-pencil" aria-hidden="true"></span>
                </button>
                </div>
                 </td>
                 </tr>
                 </tbody>
              <tfoot>

              <tr class="footable-editing">
              <td colspan="6">
              <button type="button" class="btn btn-primary footable-show" data-toggle="modal" data-target="#editor-modal">
              <span class="fooicon fooicon-pencil " aria-hidden="true"></span> <?php echo New_Warehouse ; ?></button>
              <a href="index.php"><button type="button" class="btn btn-default footable-hide"><?php echo cancel; ?></button></a>
              </td>
              </tr>
              </tfoot>
              </table>

             </div>

            
          </div>

          <? ////////////////modal for create new warehouse///////////////// ?>
          <div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-simple" role="document">
              <form class="modal-content form-horizontal" id="editor">
                
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="editor-title"><?php echo add_warehouse; ?></h4>
                </div>
                
                <div class="modal-body">
                  <input type="number" id="id" name="id" class="hidden" style="display:none;">
                  <div class="form-group required">
                    <label for="Name" class="col-sm-3 control-label "><?php echo libelle_entrepot; ?></label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="Name" name="Name" placeholder="Name" required="">
                    </div>
                  </div>

                  <div class="form-group required">
                    <label for="Adresse" class="col-sm-3 control-label"><?php echo adresse_entrepot; ?></label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="Adresse" name="Adresse" placeholder="Adresse" required="">
                    </div>
                  </div>
                  
                  <div class="form-group required">
                  <label for="manager" class="col-sm-3 control-label" ><?php echo Responsable_ware; ?></label>
                  <div class="col-sm-9">
                    <select class="custom-select" id="inputGroupSelect01" name="manager" required>
                    <option value="" selected><?php echo choosemanager; ?>...</option>
                    <?php foreach ($warehouseManager->getAll() as $b) {?>
                    <option value="<?php echo $b->getId(); ?>"><?php echo $b->getFirstname()." ".$b->getLastname(); ?></option>
                    <?php } ?>
                    </select>
                  </div>
                  </div>

                  <div class="form-group">
                  <label for="town" class="col-sm-3 control-label" >City</label>
                  <div class="col-sm-9">
                    <select class="custom-select" id="inputGroupSelect02" name="city" >
                    <option value="" selected><?php echo choosemanager; ?>...</option>
                    <?php foreach ($cities->getAll() as $a) {
                      
                      if (is_null($a->getWarehouseId())){
                      ?>

                    <option value="<?php echo $a->getId(); ?>"><?php echo $a->getName(); ?></option>
                    <?php } }?>
                    </select>
                  </div>
                  </div>


                 </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary waves-effect waves-light waves-round" id="submitSave"><?php echo save_warehouse; ?></button>
                  <button type="button" class="btn btn-default waves-effect waves-light waves-round" data-dismiss="modal"><?php echo cancel_warehouse ?></button>
                </div>
              </form>
            </div>
          </div>

          <? ////////////////end modal for create///////////////// ?>
          </div>
          </div>
        </div>
  </div>
  </div>
 </div>



  
  <!-- End Page -->


  <!-- Footer -->
  <?php include 'includes/footer.php';?>
  <!-- Core  -->
  <script src="../global/vendor/babel-external-helpers/babel-external-helpersfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jquery/jquery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/popper-js/umd/popper.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/bootstrap/bootstrap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/animsition/animsition.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/mousewheel/jquery.mousewheel.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollbar/jquery-asScrollbar.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollable/jquery-asScrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/waves/waves.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <script src="../global/vendor/jquery-mmenu/jquery.mmenu.min.allfd53.js?v4.0.1"></script>
  <script src="../global/vendor/switchery/switchery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/intro-js/intro.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/screenfull/screenfull.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/slidepanel/jquery-slidePanel.minfd53.js?v4.0.1"></script>

  <!-- Plugins For This Page -->
  <script src="../global/vendor/chartist/chartist.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/jquery-jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-enfd53.js?v4.0.1"></script>
  <script src="../global/vendor/matchheight/jquery.matchHeight-minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/peity/jquery.peity.minfd53.js?v4.0.1"></script>

  <!-- Scripts -->
  <script src="../global/js/Component.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Base.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Config.minfd53.js?v4.0.1"></script>

  <script src="assets/js/Section/Menubar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/Sidebar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/PageAside.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/GridMenu.minfd53.js?v4.0.1"></script>
  <!-- Config -->
  <script src="../global/js/config/colors.minfd53.js?v4.0.1"></script>
  <script src="assets/js/config/tour.minfd53.js?v4.0.1"></script>
  <script>
    Config.set('assets', 'assets');
  </script>

  <!-- Page -->
  <script src="assets/js/Site.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/asscrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/slidepanel.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/switchery.minfd53.js?v4.0.1"></script>

  <script src="../global/js/Plugin/matchheight.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/peity.minfd53.js?v4.0.1"></script>


  <script src="assets/examples/js/dashboard/v1.minfd53.js?v4.0.1"></script>



</body>
</html>
<?php ////// BEGIN CALL AJAX FOR CREATE NEW Warehouse /////////////// ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
 
    $("#edit-warehouse").click(function(e){
        e.preventDefault();
 
        $.post(
            'http://localhost/geoportaDev/view/mmenu/CreateWarehouse.php', 
            {
                name : $("#Name").val(),
                Adresse : $("#Adresse").val(),
                manager: $("#inputGroupSelect01").val(),
                city: $("#inputGroupSelect02").val()
            },
 
            function(dataResult){
              try{
              var dataResult = JSON.parse(dataResult);
                if(dataResult.statusCode==200){
                    alert('data added');
                    window.location.href="http://localhost/geoportaDev/view/mmenu/Warehouse.php";
                }
                else{
                    alert('failed');
                }
         
            }
            catch (e) {
              console.error(e)
              console.error('JSON recived :', dataResult)
                }
            
            }
            ,
            'text'
           
         );
    });
});
</script>
<?php ////// END CALL AJAX FOR CREATE NEW Warehouse /////////////// ?>
<?php ////////////////////////////////////////////////////////////// ?>
