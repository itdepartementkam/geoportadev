<?php
//page client
define( 'FirstName', 'Nom' );
define( 'LastName', 'Prenom' );
define( 'Address', 'Addresse' );
define( 'Phone1', 'Mobile 1' );
define( 'Phone2', 'Mobile 2' );
define( 'ListCustomers', 'Lists Clients' );
define( 'AddCustomer', 'Add Client' );
define( 'update', 'Modifier' );
define( 'Actions', 'Actions' );
/************************vehicule******* */
//***********************Vehicule *********** */
define( 'Number', 'Numero' );
define( 'Brands', 'Marque' );
define( 'Status', 'Statut' );
define( 'Fuel', 'Carburant' );
//***********************warehouse.php *****/
define( 'title_warehouse', 'Afficher tous les entrepôts' );
define( 'show_warehouse', 'Listes des entrepôts' );
define( 'name_warehouse', 'Nom de l"entrepôt' );
define( 'adresse', 'Adresse' );
define( 'New_Warehouse', 'Créer un entrepôt' );
define( 'Edit_warehouse', 'Editer un entrepôt' );
define( 'Adress', 'Adresse' );
define( 'Town', 'Ville' );
define( 'UserEmail', 'EmailClient' );
define( 'Salary', 'Salaire' );
define( 'add_warehouse', 'Ajouter un nouveau entrepôt' );
define( 'libelle_entrepot', 'Nom' );
define( 'adresse_entrepot', 'Adresse' );
define( 'Responsable_ware', 'Responsable' );
define( 'choosemanager', 'selectionner' );
define( 'save_warehouse', 'Save changes' );
define( 'cancel_warehouse', 'fermer' );

//*****************************updateWarehouse.php******/
define( 'update_warehouse', 'Modifier un entrepôt' );
define( 'adresse_warehouse', 'Adresse' );
define( 'save', 'Enregistrer' );
define( 'back', 'retour' );

//***************Town.php**************/
define( 'title_town', 'Afficher tous les villes' );
define( 'Name_town', 'Nom' );
define( 'Zip_code', 'Zip code' );
define( 'Country', 'Pays' );
define( 'Warehouse', 'Entrepôt' );
define( 'Created_at', 'Date de création' );
define( 'show_city', 'Affciher ville' );
define( 'Search', 'Rechercher' );
define( 'Edit_City', 'Editer ville' );
define( 'New_City', 'Nouveau ville' );
define( 'cancel', 'Fermer' );
//***************Package.php+dimension.php+drive.php**************/
$ListePackages = 'Liste Packages';
$Addpackage = 'Ajouter Colis';
$Customer = 'Client';
$Destination = 'Destination';
$Namepackage = 'Nom du colis';
$Fragile = 'Fragile';
$Weight = 'Poids';
$Price = 'Prix';
$BarreCode = 'Code à Barre ';
$Listedrive = 'Liste des transporteurs';
$ADD = 'Ajouter';
$Actions = 'Actions';
$VehicleNumber = 'Numéro du Vehicle';
$transporterEmail = 'Email du Transporteur ';
$Width = 'Largeur';
$Create_time = 'Creation date';
$Listdimension = 'Liste des  Dimensions';
$Adddimension = 'Ajouter une Dimension';
$Name = 'Nom';
$height = 'Longueur';
$length = 'length';
//***************country.php+user.php+language.php**************/
$name = 'Name';
$description = 'Description';
$customer = 'Customer';
$barcode = 'Barcode';
$phone_code = 'Phone code';
$destination = 'Destination Adress';
$npackage = 'New Package';
$update = 'update';
$action = 'Actions';
$create_time = 'Creation date';
$email = 'Email';
$role = 'Role';
$addCountry = 'Add Country';
$listeCountries = 'Liste Countries';
$addUser = 'Add User';
$listeUsers = 'Users List';
$language = 'Language';
$customer = 'Client';
$package = 'Colis';
$commentaire = 'Commentaire';
$status = 'Statut';
$Addreclamation = 'Ajouter une  Reclamation';
$languge = 'Language';
$City = 'Ville';
$Responsable = 'Responsable';
$warehouse = 'depot';
$firstName = 'Prénom';
$updateTrasporter = 'Modifier les données du livreur';
$lastName = 'Nom';
$password = 'Mot de passe';
$salary = 'Salaire';
$Phone2 = 'Téléphone 2';
$Phone1 = 'Téléphone 1';
$listTransporters = 'Liste des livreurs';
$addTransporter = 'Ajouter Livreur';
$ListWarehouseManager = 'Liste responsable warehouse';
$AddWarehouseManager = 'Ajouter responsable'
?>
