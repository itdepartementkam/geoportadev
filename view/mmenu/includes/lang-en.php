<?php
//***********************Customer ***************/
define( 'FirstName', 'First Name' );
define( 'LastName', 'Last Name' );
define( 'Address', 'Address' );
define( 'Phone1', 'Phone 1' );
define( 'Phone2', 'Phone 2' );
define( 'ListCustomers', 'List Customers' );
define( 'AddCustomer', 'Add Customer' );
define( 'update', 'Update' );
define( 'Actions', 'Actions' );
//***********************Vehicule *********** */
define( 'Number', 'Number' );
define( 'Brands', 'Brands' );
define( 'Status', 'Status' );
define( 'Fuel', 'Fuel' );
//***********************warehouse.php *****/
define( 'title_warehouse', 'Show all warehouses' );
define( 'show_warehouse', 'Show warehouse' );
define( 'name_warehouse', 'Name of warehouse' );
define( 'adresse', 'Adresse' );
define( 'New_Warehouse', 'New Warehouse' );
define( 'Edit_warehouse', 'Edit Warehouse' );

//*****************************updateWarehouse.php******/
define( 'update_warehouse', 'Update warehouse' );
define( 'adresse_warehouse', 'Adresse' );
define( 'save', 'save' );
define( 'back', 'back' );

//***************Town.php**************/
define( 'title_town', 'Show all the towns' );
define( 'Name_town', 'Name' );
define( 'Zip_code', 'Zip code' );
define( 'Country', 'Country' );
define( 'Warehouse', 'Warehouse' );
define( 'Created_at', 'Created at' );
define( 'show_city', 'Show City' );
define( 'Search', 'Search' );
define( 'Edit_City', 'Edit City' );
define( 'New_City', 'New City' );
define( 'cancel', 'Cancel' );

///////////////*  */
//***************Package.php+dimension.php+drive.php**************/
$ListePackages = '*******';
$Addpackage = 'Add package';
$Destination = 'Destination';
$Namepackage = 'Name package';
$Fragile = 'Fragile';
$Weight = 'Weight';
$Price = 'Price';
$BarreCode = 'Barre Code';
$Listedrive = 'List drives';
$ADD = 'ADD';
$Actions = 'Actions';
$VehicleNumber = 'Vehicle Number';
$transporterEmail = 'Transporter Email';
$Width = 'Width';
$Create_time = 'Create_time';
$Listdimension = 'List Dimension';
$Adddimension = 'Add Dimension';
$Name = 'Name';
$height = 'Height';
$length = 'length';
//***************country.php+user.php+language.php**************/
$name = 'Name';
$description = 'Description';
$customer = 'Customer';
$barcode = 'Barcode';
$phone_code = 'Phone code';
$destination = 'Destination Adress';
$npackage = 'New Package';
$update = 'update';
$action = 'Actions';
$create_time = 'Creation date';
$email = 'Email';
$role = 'Role';
$addCountry = 'Add Country';
$listeCountries = 'Countries List';
$addUser = 'Add User';
$listeUsers = 'Users List';
$language = 'Language';
$package = 'Package';
$commentaire = 'Comment';
$status = 'Status';
$Addreclamation = 'Add Reclamation';
$languge = 'Language';
$City = 'City';
$Responsable = 'Managr';
$warehouse = 'warehouse';
$ListWarehouseManager = 'List Warehouse Manager';
$AddWarehouseManager="Add Warehouse Manager"
?>
