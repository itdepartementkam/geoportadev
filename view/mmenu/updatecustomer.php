<?php include_once("includes/verif-lang.php") ?>
<?php require("../../models/User.php"); require("../../models/City.php"); require("../../models/Language.php"); require('../../models/Customer.php');?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:33:04 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Dashboard</title>

  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="../global/css/bootstrap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/css/bootstrap-extend.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="assets/css/site.minfd53.css?v4.0.1">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="../global/css/skintools.minfd53.css?v4.0.1">
  <script src="assets/js/Plugin/skintools.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="../global/vendor/animsition/animsition.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/asscrollable/asScrollable.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/switchery/switchery.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/intro-js/introjs.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/slidepanel/slidePanel.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jquery-mmenu/jquery-mmenu.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/flag-icon-css/flag-icon.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/waves/waves.minfd53.css?v4.0.1">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="../global/vendor/chartist/chartist.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jvectormap/jquery-jvectormap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.css?v4.0.1">

  <!-- Page -->
  <link rel="stylesheet" href="assets/examples/css/dashboard/v1.minfd53.css?v4.0.1">

  <!-- Fonts -->
  <link rel="stylesheet" href="../global/fonts/material-design/material-design.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/fonts/brand-icons/brand-icons.minfd53.css?v4.0.1">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">


  <!-- Scripts -->
  <script src="../global/vendor/breakpoints/breakpoints.minfd53.js?v4.0.1"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="animsition site-navbar-small dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    
<?php
  $id=(int) $_GET["id"];
$customer=new Customer($id); 
?>
 <?php include 'includes/navbar.php';?>
 <?php include 'includes/menu.php';?>


  <!-- Page -->
  <div class="page">
 <!-- Panel Full Example -->
 <div class="page-content container-fluid">
 <div class="col-md-12">
          <!-- Panel Static Labels -->
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Update customer <?php echo $customer->getFirstname()?></h3>
            </div>
            <div class="panel-body container-fluid">
              <form  method="post">
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputText">Firstname</label>
                  <input type="text" class="form-control" id="inputText" name="Firstname"  value="<?=$customer->getFirstname()?>"/>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputEmail">Lastname</label>
                  <input type="text" class="form-control" id="inputEmail" name="Lastname" value="<?=$customer->getLastname()?>" />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputEmail">Address</label>
                  <input type="text" class="form-control" id="inputEmail" name="Address" value="<?=$customer->getAddress()?>" />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputEmail">Phone1</label>
                  <input type="text" class="form-control" id="inputEmail" name="Phone1" value="<?=$customer->getPhone1()?>" />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputEmail">Phone2</label>
                  <input type="text" class="form-control" id="inputEmail" name="Phone2" value="<?=$customer->getPhone2()?>" />
                </div>
                 <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="select">language</label>
                  <select class="form-control" id="select" name="language">
                  <?php   
                             $language=new Language(); 
                             foreach ($language->getAll() as $lan)
                      {
                             $newlan=$lan->getId();             
                              echo "<option value='$newlan'>".$lan->getName()."</option>";
                              
                     }      ?>     
    
                  </select>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="select">city</label>
                  <select class="form-control" id="select" name="City">
                  <?php   
                             $city=new City(); 
                             foreach ($city->getAll() as $ci)
                      {
                             $newci=$ci->getId();             
                              echo "<option value='$newci'>".$ci->getName()."</option>";
                              
                     }      ?>     
    
                  </select>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="select">user</label>
                  <select class="form-control" id="select" name="User" >
                  <?php   
                             $user=new User(); 
                             foreach ($user->getAll() as $us)
                      {
                             $newus=$us->getId();             
                              echo "<option value='$newus'>".$us->getEmail()."</option>";
                              
                     }      ?>     
    
                  </select>
                </div>
               
                
                
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info" name="btn" type="button">Update</button>
                    </span>
                  </div>
                </div>
                
              </form>
              
		<?php



if(isset($_POST['btn'])){
  extract($_POST);
  $customer->setFirstname($Firstname);
  $customer->setLastname($Lastname);
  $customer->setAddress($Address);
  $customer->setPhone1($Phone1);
  $customer->setPhone2($Phone2);
  $customer->setIdLanguge($Languge);
  $customer->setCityId($City);
  $customer->setUserId($User);



  
  $customer->update((int)$customer->getId());



} 
 
?>
            </div>
          </div>
          <!-- End Panel Static Labels -->
        </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <?php include 'includes/footer.php';?>
  <!-- Core  -->
  <script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="../global/vendor/babel-external-helpers/babel-external-helpersfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jquery/jquery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/popper-js/umd/popper.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/bootstrap/bootstrap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/animsition/animsition.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/mousewheel/jquery.mousewheel.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollbar/jquery-asScrollbar.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollable/jquery-asScrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/waves/waves.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <script src="../global/vendor/jquery-mmenu/jquery.mmenu.min.allfd53.js?v4.0.1"></script>
  <script src="../global/vendor/switchery/switchery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/intro-js/intro.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/screenfull/screenfull.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/slidepanel/jquery-slidePanel.minfd53.js?v4.0.1"></script>

  <!-- Plugins For This Page -->
  <script src="../global/vendor/chartist/chartist.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/jquery-jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-enfd53.js?v4.0.1"></script>
  <script src="../global/vendor/matchheight/jquery.matchHeight-minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/peity/jquery.peity.minfd53.js?v4.0.1"></script>

  <!-- Scripts -->
  <script src="../global/js/Component.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Base.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Config.minfd53.js?v4.0.1"></script>

  <script src="assets/js/Section/Menubar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/Sidebar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/PageAside.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/GridMenu.minfd53.js?v4.0.1"></script>
  <!-- Config -->
  <script src="../global/js/config/colors.minfd53.js?v4.0.1"></script>
  <script src="assets/js/config/tour.minfd53.js?v4.0.1"></script>
  <script>
    Config.set('assets', 'assets');
  </script>

  <!-- Page -->
  <script src="assets/js/Site.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/asscrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/slidepanel.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/switchery.minfd53.js?v4.0.1"></script>

  <script src="../global/js/Plugin/matchheight.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/peity.minfd53.js?v4.0.1"></script>


  <script src="assets/examples/js/dashboard/v1.minfd53.js?v4.0.1"></script>


</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:34:21 GMT -->
</html>