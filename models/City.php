<?php

require_once 'DAO/City.dao.php';

/**
 * Class City
 */
class City extends CityDAO
{
  // PUT YOUR BUSINESS LOGIC HERE
   /**
   * All Cities - referred table
   * @returns object[]
   */
  public function getAll()
  {
    $sql="SELECT * FROM city";
    return $this->getObjects($sql,'City');
  }
}


