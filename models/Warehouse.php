<?php

require_once 'DAO/Warehouse.dao.php';

/**
 * Class Warehouse
 */
class Warehouse extends WarehouseDAO
{
  // PUT YOUR BUSINESS LOGIC HERE
   /**
   * All Warehouses - referred table
   * @returns object[]
   */
  public function getAll()
  {
    $sql="SELECT * FROM warehouse";
    return $this->getObjects($sql,'Warehouse');
  }
}


