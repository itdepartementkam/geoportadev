<?php

require_once 'DAO.php';

/**
 * Class LocalisationDAO
*/
abstract class LocalisationDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->package.id
   * @var int $package_id
   */
  protected $package_id;

  public function getPackageId() {return $this->package_id;}

  public function setPackageId($package_id) {$this->package_id=$package_id;}

  /**
   * Protected variable
   * (FK)->transporter.id
   * @var int $transporter_id
   */
  protected $transporter_id;

  public function getTransporterId() {return $this->transporter_id;}

  public function setTransporterId($transporter_id) {$this->transporter_id=$transporter_id;}

  /**
   * Protected variable
   * (FK)->vehicle.id
   * @var int $id_vehicle
   */
  protected $id_vehicle;

  public function getIdVehicle() {return $this->id_vehicle;}

  public function setIdVehicle($id_vehicle) {$this->id_vehicle=$id_vehicle;}

  /**
   * Protected variable
   * (FK)->warehouse.id
   * @var int $warehouse_id
   */
  protected $warehouse_id;

  public function getWarehouseId() {return $this->warehouse_id;}

  public function setWarehouseId($warehouse_id) {$this->warehouse_id=$warehouse_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='localisation';
    $this->primkeys=['id'];
    $this->fields=['package_id','transporter_id','id_vehicle','warehouse_id'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Package - referenced table
   * @returns object
   */
  public function getPackage()
  {
    $sql="SELECT * FROM package WHERE id='{$this->package_id}' LIMIT 1";
    return $this->getObject($sql,'Package');
  }

  /**
   * Transporter - referenced table
   * @returns object
   */
  public function getTransporter()
  {
    $sql="SELECT * FROM transporter WHERE id='{$this->transporter_id}' LIMIT 1";
    return $this->getObject($sql,'Transporter');
  }

  /**
   * Vehicle - referenced table
   * @returns object
   */
  public function getVehicle()
  {
    $sql="SELECT * FROM vehicle WHERE id='{$this->id_vehicle}' LIMIT 1";
    return $this->getObject($sql,'Vehicle');
  }

  /**
   * Warehouse - referenced table
   * @returns object
   */
  public function getWarehouse()
  {
    $sql="SELECT * FROM warehouse WHERE id='{$this->warehouse_id}' LIMIT 1";
    return $this->getObject($sql,'Warehouse');
  }

  /**
   * Column package_id Finder
   * @return object[]
   */
  public function findByPackageId($package_id)
  {
    $sql="SELECT * FROM localisation WHERE package_id='$package_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column transporter_id Finder
   * @return object[]
   */
  public function findByTransporterId($transporter_id)
  {
    $sql="SELECT * FROM localisation WHERE transporter_id='$transporter_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_vehicle Finder
   * @return object[]
   */
  public function findByIdVehicle($id_vehicle)
  {
    $sql="SELECT * FROM localisation WHERE id_vehicle='$id_vehicle'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column warehouse_id Finder
   * @return object[]
   */
  public function findByWarehouseId($warehouse_id)
  {
    $sql="SELECT * FROM localisation WHERE warehouse_id='$warehouse_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM localisation WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

