<?php

require_once 'DAO.php';

/**
 * Class TransporterDAO
*/
abstract class TransporterDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->language.id
   * @var int $id_language
   */
  protected $id_language;

  public function getIdLanguage() {return $this->id_language;}

  public function setIdLanguage($id_language) {$this->id_language=$id_language;}

  /**
   * Protected variable
   * (FK)->user.id
   * @var int $user_id
   */
  protected $user_id;

  public function getUserId() {return $this->user_id;}

  public function setUserId($user_id) {$this->user_id=$user_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * @var varchar $firstname
   */
  protected $firstname;

  public function getFirstname() {return $this->firstname;}

  public function setFirstname($firstname) {$this->firstname=$firstname;}

  /**
   * Protected variable
   * @var varchar $lastname
   */
  protected $lastname;

  public function getLastname() {return $this->lastname;}

  public function setLastname($lastname) {$this->lastname=$lastname;}

  /**
   * Protected variable
   * @var float $salary
   */
  protected $salary;

  public function getSalary() {return $this->salary;}

  public function setSalary($salary) {$this->salary=$salary;}

  /**
   * Protected variable
   * @var varchar $phone1
   */
  protected $phone1;

  public function getPhone1() {return $this->phone1;}

  public function setPhone1($phone1) {$this->phone1=$phone1;}

  /**
   * Protected variable
   * @var varchar $phone2
   */
  protected $phone2;

  public function getPhone2() {return $this->phone2;}

  public function setPhone2($phone2) {$this->phone2=$phone2;}

  /**
   * Protected variable
   * @var timestamp $create_time
   */
  protected $create_time;

  public function getCreateTime() {return $this->create_time;}

  public function setCreateTime($create_time) {$this->create_time=$create_time;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='transporter';
    $this->primkeys=['id'];
    $this->fields=['id_language','user_id','firstname','lastname','salary','phone1','phone2','create_time'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Language - referenced table
   * @returns object
   */
  public function getLanguage()
  {
    $sql="SELECT * FROM language WHERE id='{$this->id_language}' LIMIT 1";
    return $this->getObject($sql,'Language');
  }

  /**
   * User - referenced table
   * @returns object
   */
  public function getUser()
  {
    $sql="SELECT * FROM user WHERE id='{$this->user_id}' LIMIT 1";
    return $this->getObject($sql,'User');
  }

  /**
   * Drives - referred table
   * @returns object[]
   */
  public function getDrives()
  {
    $sql="SELECT * FROM drive WHERE transporter_id='{$this->id}'";
    return $this->getObjects($sql,'Drive');
  }

  /**
   * Localisations - referred table
   * @returns object[]
   */
  public function getLocalisations()
  {
    $sql="SELECT * FROM localisation WHERE transporter_id='{$this->id}'";
    return $this->getObjects($sql,'Localisation');
  }

  /**
   * Column id_language Finder
   * @return object[]
   */
  public function findByIdLanguage($id_language)
  {
    $sql="SELECT * FROM transporter WHERE id_language='$id_language'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column user_id Finder
   * @return object[]
   */
  public function findByUserId($user_id)
  {
    $sql="SELECT * FROM transporter WHERE user_id='$user_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM transporter WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column firstname Finder
   * @return object[]
   */
  public function findByFirstname($firstname)
  {
    $sql="SELECT * FROM transporter WHERE firstname='$firstname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column lastname Finder
   * @return object[]
   */
  public function findByLastname($lastname)
  {
    $sql="SELECT * FROM transporter WHERE lastname='$lastname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column salary Finder
   * @return object[]
   */
  public function findBySalary($salary)
  {
    $sql="SELECT * FROM transporter WHERE salary='$salary'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column phone1 Finder
   * @return object[]
   */
  public function findByPhone1($phone1)
  {
    $sql="SELECT * FROM transporter WHERE phone1='$phone1'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column phone2 Finder
   * @return object[]
   */
  public function findByPhone2($phone2)
  {
    $sql="SELECT * FROM transporter WHERE phone2='$phone2'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column create_time Finder
   * @return object[]
   */
  public function findByCreateTime($create_time)
  {
    $sql="SELECT * FROM transporter WHERE create_time='$create_time'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

