<?php

require_once 'DAO.php';

/**
 * Class CustomerDAO
*/
abstract class CustomerDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->city.id
   * @var int $city_id
   */
  protected $city_id;

  public function getCityId() {return $this->city_id;}

  public function setCityId($city_id) {$this->city_id=$city_id;}

  /**
   * Protected variable
   * (FK)->language.id
   * @var int $id_languge
   */
  protected $id_languge;

  public function getIdLanguge() {return $this->id_languge;}

  public function setIdLanguge($id_languge) {$this->id_languge=$id_languge;}

  /**
   * Protected variable
   * (FK)->user.id
   * @var int $user_id
   */
  protected $user_id;

  public function getUserId() {return $this->user_id;}

  public function setUserId($user_id) {$this->user_id=$user_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * @var varchar $firstname
   */
  protected $firstname;

  public function getFirstname() {return $this->firstname;}

  public function setFirstname($firstname) {$this->firstname=$firstname;}

  /**
   * Protected variable
   * @var varchar $lastname
   */
  protected $lastname;

  public function getLastname() {return $this->lastname;}

  public function setLastname($lastname) {$this->lastname=$lastname;}

  /**
   * Protected variable
   * @var varchar $address
   */
  protected $address;

  public function getAddress() {return $this->address;}

  public function setAddress($address) {$this->address=$address;}

  /**
   * Protected variable
   * @var varchar $phone1
   */
  protected $phone1;

  public function getPhone1() {return $this->phone1;}

  public function setPhone1($phone1) {$this->phone1=$phone1;}

  /**
   * Protected variable
   * @var varchar $phone2
   */
  protected $phone2;

  public function getPhone2() {return $this->phone2;}

  public function setPhone2($phone2) {$this->phone2=$phone2;}

  /**
   * Protected variable
   * @var timestamp $create_time
   */
  protected $create_time;

  public function getCreateTime() {return $this->create_time;}

  public function setCreateTime($create_time) {$this->create_time=$create_time;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='customer';
    $this->primkeys=['id'];
    $this->fields=['city_id','id_languge','user_id','firstname','lastname','address','phone1','phone2','create_time'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * City - referenced table
   * @returns object
   */
  public function getCity()
  {
    $sql="SELECT * FROM city WHERE id='{$this->city_id}' LIMIT 1";
    return $this->getObject($sql,'City');
  }

  /**
   * Language - referenced table
   * @returns object
   */
  public function getLanguage()
  {
    $sql="SELECT * FROM language WHERE id='{$this->id_languge}' LIMIT 1";
    return $this->getObject($sql,'Language');
  }

  /**
   * User - referenced table
   * @returns object
   */
  public function getUser()
  {
    $sql="SELECT * FROM user WHERE id='{$this->user_id}' LIMIT 1";
    return $this->getObject($sql,'User');
  }

  /**
   * Admins - referred table
   * @returns object[]
   */
  public function getAdmins()
  {
    $sql="SELECT * FROM admin WHERE id_client='{$this->id}'";
    return $this->getObjects($sql,'Admin');
  }

  /**
   * Packages - referred table
   * @returns object[]
   */
  public function getPackages()
  {
    $sql="SELECT * FROM package WHERE customer_id='{$this->id}'";
    return $this->getObjects($sql,'Package');
  }

  /**
   * Reclamations - referred table
   * @returns object[]
   */
  public function getReclamations()
  {
    $sql="SELECT * FROM reclamation WHERE customer_id='{$this->id}'";
    return $this->getObjects($sql,'Reclamation');
  }

  /**
   * Column city_id Finder
   * @return object[]
   */
  public function findByCityId($city_id)
  {
    $sql="SELECT * FROM customer WHERE city_id='$city_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_languge Finder
   * @return object[]
   */
  public function findByIdLanguge($id_languge)
  {
    $sql="SELECT * FROM customer WHERE id_languge='$id_languge'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column user_id Finder
   * @return object[]
   */
  public function findByUserId($user_id)
  {
    $sql="SELECT * FROM customer WHERE user_id='$user_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM customer WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column firstname Finder
   * @return object[]
   */
  public function findByFirstname($firstname)
  {
    $sql="SELECT * FROM customer WHERE firstname='$firstname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column lastname Finder
   * @return object[]
   */
  public function findByLastname($lastname)
  {
    $sql="SELECT * FROM customer WHERE lastname='$lastname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column address Finder
   * @return object[]
   */
  public function findByAddress($address)
  {
    $sql="SELECT * FROM customer WHERE address='$address'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column phone1 Finder
   * @return object[]
   */
  public function findByPhone1($phone1)
  {
    $sql="SELECT * FROM customer WHERE phone1='$phone1'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column phone2 Finder
   * @return object[]
   */
  public function findByPhone2($phone2)
  {
    $sql="SELECT * FROM customer WHERE phone2='$phone2'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column create_time Finder
   * @return object[]
   */
  public function findByCreateTime($create_time)
  {
    $sql="SELECT * FROM customer WHERE create_time='$create_time'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

