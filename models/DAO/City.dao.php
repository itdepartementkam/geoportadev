<?php

require_once 'DAO.php';

/**
 * Class CityDAO
*/
abstract class CityDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->country.id
   * @var int $country_id
   */
  protected $country_id;

  public function getCountryId() {return $this->country_id;}

  public function setCountryId($country_id) {$this->country_id=$country_id;}

  /**
   * Protected variable
   * (FK)->warehouse.id
   * @var int $warehouse_id
   */
  protected $warehouse_id;

  public function getWarehouseId() {return $this->warehouse_id;}

  public function setWarehouseId($warehouse_id) {$this->warehouse_id=$warehouse_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $name
   */
  protected $name;

  public function getName() {return $this->name;}

  public function setName($name) {$this->name=$name;}

  /**
   * Protected variable
   * @var int $zipcode
   */
  protected $zipcode;

  public function getZipcode() {return $this->zipcode;}

  public function setZipcode($zipcode) {$this->zipcode=$zipcode;}

  /**
   * Protected variable
   * @var timestamp $create_time
   */
  protected $create_time;

  public function getCreateTime() {return $this->create_time;}

  public function setCreateTime($create_time) {$this->create_time=$create_time;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='city';
    $this->primkeys=['id'];
    $this->fields=['country_id','warehouse_id','name','zipcode','create_time'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Country - referenced table
   * @returns object
   */
  public function getCountry()
  {
    $sql="SELECT * FROM country WHERE id='{$this->country_id}' LIMIT 1";
    return $this->getObject($sql,'Country');
  }

  /**
   * Warehouse - referenced table
   * @returns object
   */
  public function getWarehouse()
  {
    $sql="SELECT * FROM warehouse WHERE id='{$this->warehouse_id}' LIMIT 1";
    return $this->getObject($sql,'Warehouse');
  }

  /**
   * Admins - referred table
   * @returns object[]
   */
  public function getAdmins()
  {
    $sql="SELECT * FROM admin WHERE id_city='{$this->id}'";
    return $this->getObjects($sql,'Admin');
  }

  /**
   * Customers - referred table
   * @returns object[]
   */
  public function getCustomers()
  {
    $sql="SELECT * FROM customer WHERE city_id='{$this->id}'";
    return $this->getObjects($sql,'Customer');
  }

  /**
   * Destinations - referred table
   * @returns object[]
   */
  public function getDestinations()
  {
    $sql="SELECT * FROM destination WHERE city_id='{$this->id}'";
    return $this->getObjects($sql,'Destination');
  }

  /**
   * Warehousemanegers - referred table
   * @returns object[]
   */
  public function getWarehousemanegers()
  {
    $sql="SELECT * FROM warehousemaneger WHERE id_city='{$this->id}'";
    return $this->getObjects($sql,'Warehousemaneger');
  }

  /**
   * Column country_id Finder
   * @return object[]
   */
  public function findByCountryId($country_id)
  {
    $sql="SELECT * FROM city WHERE country_id='$country_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column warehouse_id Finder
   * @return object[]
   */
  public function findByWarehouseId($warehouse_id)
  {
    $sql="SELECT * FROM city WHERE warehouse_id='$warehouse_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM city WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByName($name)
  {
    $sql="SELECT * FROM city WHERE name='$name' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column zipcode Finder
   * @return object[]
   */
  public function findByZipcode($zipcode)
  {
    $sql="SELECT * FROM city WHERE zipcode='$zipcode'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column create_time Finder
   * @return object[]
   */
  public function findByCreateTime($create_time)
  {
    $sql="SELECT * FROM city WHERE create_time='$create_time'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

