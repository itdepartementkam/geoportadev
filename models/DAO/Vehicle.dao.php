<?php

require_once 'DAO.php';

/**
 * Class VehicleDAO
*/
abstract class VehicleDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->warehouse.id
   * @var int $warehouse_id
   */
  protected $warehouse_id;

  public function getWarehouseId() {return $this->warehouse_id;}

  public function setWarehouseId($warehouse_id) {$this->warehouse_id=$warehouse_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $number
   */
  protected $number;

  public function getNumber() {return $this->number;}

  public function setNumber($number) {$this->number=$number;}

  /**
   * Protected variable
   * @var varchar $brands
   */
  protected $brands;

  public function getBrands() {return $this->brands;}

  public function setBrands($brands) {$this->brands=$brands;}

  /**
   * Protected variable
   * @var varchar $status
   */
  protected $status;

  public function getStatus() {return $this->status;}

  public function setStatus($status) {$this->status=$status;}

  /**
   * Protected variable
   * @var float $longitude
   */
  protected $longitude;

  public function getLongitude() {return $this->longitude;}

  public function setLongitude($longitude) {$this->longitude=$longitude;}

  /**
   * Protected variable
   * @var decimal $latitude
   */
  protected $latitude;

  public function getLatitude() {return $this->latitude;}

  public function setLatitude($latitude) {$this->latitude=$latitude;}

  /**
   * Protected variable
   * @var varchar $fuel
   */
  protected $fuel;

  public function getFuel() {return $this->fuel;}

  public function setFuel($fuel) {$this->fuel=$fuel;}

  /**
   * Protected variable
   * @var varchar $type
   */
  protected $type;

  public function getType() {return $this->type;}

  public function setType($type) {$this->type=$type;}

  /**
   * Protected variable
   * @var float $wheight
   */
  protected $wheight;

  public function getWheight() {return $this->wheight;}

  public function setWheight($wheight) {$this->wheight=$wheight;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='vehicle';
    $this->primkeys=['id'];
    $this->fields=['warehouse_id','number','brands','status','longitude','latitude','fuel','type','wheight'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Warehouse - referenced table
   * @returns object
   */
  public function getWarehouse()
  {
    $sql="SELECT * FROM warehouse WHERE id='{$this->warehouse_id}' LIMIT 1";
    return $this->getObject($sql,'Warehouse');
  }

  /**
   * Drives - referred table
   * @returns object[]
   */
  public function getDrives()
  {
    $sql="SELECT * FROM drive WHERE vehicle_id='{$this->id}'";
    return $this->getObjects($sql,'Drive');
  }

  /**
   * Localisations - referred table
   * @returns object[]
   */
  public function getLocalisations()
  {
    $sql="SELECT * FROM localisation WHERE id_vehicle='{$this->id}'";
    return $this->getObjects($sql,'Localisation');
  }

  /**
   * Column warehouse_id Finder
   * @return object[]
   */
  public function findByWarehouseId($warehouse_id)
  {
    $sql="SELECT * FROM vehicle WHERE warehouse_id='$warehouse_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM vehicle WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByNumber($number)
  {
    $sql="SELECT * FROM vehicle WHERE number='$number' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column brands Finder
   * @return object[]
   */
  public function findByBrands($brands)
  {
    $sql="SELECT * FROM vehicle WHERE brands='$brands'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column status Finder
   * @return object[]
   */
  public function findByStatus($status)
  {
    $sql="SELECT * FROM vehicle WHERE status='$status'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column longitude Finder
   * @return object[]
   */
  public function findByLongitude($longitude)
  {
    $sql="SELECT * FROM vehicle WHERE longitude='$longitude'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column latitude Finder
   * @return object[]
   */
  public function findByLatitude($latitude)
  {
    $sql="SELECT * FROM vehicle WHERE latitude='$latitude'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column fuel Finder
   * @return object[]
   */
  public function findByFuel($fuel)
  {
    $sql="SELECT * FROM vehicle WHERE fuel='$fuel'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column type Finder
   * @return object[]
   */
  public function findByType($type)
  {
    $sql="SELECT * FROM vehicle WHERE type='$type'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column wheight Finder
   * @return object[]
   */
  public function findByWheight($wheight)
  {
    $sql="SELECT * FROM vehicle WHERE wheight='$wheight'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

