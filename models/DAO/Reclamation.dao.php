<?php

require_once 'DAO.php';

/**
 * Class ReclamationDAO
*/
abstract class ReclamationDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->customer.id
   * @var int $customer_id
   */
  protected $customer_id;

  public function getCustomerId() {return $this->customer_id;}

  public function setCustomerId($customer_id) {$this->customer_id=$customer_id;}

  /**
   * Protected variable
   * (FK)->package.id
   * @var int $package_id
   */
  protected $package_id;

  public function getPackageId() {return $this->package_id;}

  public function setPackageId($package_id) {$this->package_id=$package_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * @var varchar $commentaire
   */
  protected $commentaire;

  public function getCommentaire() {return $this->commentaire;}

  public function setCommentaire($commentaire) {$this->commentaire=$commentaire;}

  /**
   * Protected variable
   * @var varchar $status
   */
  protected $status;

  public function getStatus() {return $this->status;}

  public function setStatus($status) {$this->status=$status;}

  /**
   * Protected variable
   * @var timestamp $create_time
   */
  protected $create_time;

  public function getCreateTime() {return $this->create_time;}

  public function setCreateTime($create_time) {$this->create_time=$create_time;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='reclamation';
    $this->primkeys=['id'];
    $this->fields=['customer_id','package_id','commentaire','status','create_time'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Customer - referenced table
   * @returns object
   */
  public function getCustomer()
  {
    $sql="SELECT * FROM customer WHERE id='{$this->customer_id}' LIMIT 1";
    return $this->getObject($sql,'Customer');
  }

  /**
   * Package - referenced table
   * @returns object
   */
  public function getPackage()
  {
    $sql="SELECT * FROM package WHERE id='{$this->package_id}' LIMIT 1";
    return $this->getObject($sql,'Package');
  }

  /**
   * Column customer_id Finder
   * @return object[]
   */
  public function findByCustomerId($customer_id)
  {
    $sql="SELECT * FROM reclamation WHERE customer_id='$customer_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column package_id Finder
   * @return object[]
   */
  public function findByPackageId($package_id)
  {
    $sql="SELECT * FROM reclamation WHERE package_id='$package_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM reclamation WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column commentaire Finder
   * @return object[]
   */
  public function findByCommentaire($commentaire)
  {
    $sql="SELECT * FROM reclamation WHERE commentaire='$commentaire'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column status Finder
   * @return object[]
   */
  public function findByStatus($status)
  {
    $sql="SELECT * FROM reclamation WHERE status='$status'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column create_time Finder
   * @return object[]
   */
  public function findByCreateTime($create_time)
  {
    $sql="SELECT * FROM reclamation WHERE create_time='$create_time'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

