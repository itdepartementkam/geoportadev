<?php

require_once 'DAO.php';

/**
 * Class DestinationDAO
*/
abstract class DestinationDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->city.id
   * @var int $city_id
   */
  protected $city_id;

  public function getCityId() {return $this->city_id;}

  public function setCityId($city_id) {$this->city_id=$city_id;}

  /**
   * Protected variable
   * (FK)->language.id
   * @var int $id_language
   */
  protected $id_language;

  public function getIdLanguage() {return $this->id_language;}

  public function setIdLanguage($id_language) {$this->id_language=$id_language;}

  /**
   * Protected variable
   * (FK)->user.id
   * @var int $id_user
   */
  protected $id_user;

  public function getIdUser() {return $this->id_user;}

  public function setIdUser($id_user) {$this->id_user=$id_user;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * @var varchar $firstname
   */
  protected $firstname;

  public function getFirstname() {return $this->firstname;}

  public function setFirstname($firstname) {$this->firstname=$firstname;}

  /**
   * Protected variable
   * @var varchar $lastname
   */
  protected $lastname;

  public function getLastname() {return $this->lastname;}

  public function setLastname($lastname) {$this->lastname=$lastname;}

  /**
   * Protected variable
   * @var varchar $address
   */
  protected $address;

  public function getAddress() {return $this->address;}

  public function setAddress($address) {$this->address=$address;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='destination';
    $this->primkeys=['id'];
    $this->fields=['city_id','id_language','id_user','firstname','lastname','address'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * City - referenced table
   * @returns object
   */
  public function getCity()
  {
    $sql="SELECT * FROM city WHERE id='{$this->city_id}' LIMIT 1";
    return $this->getObject($sql,'City');
  }

  /**
   * Language - referenced table
   * @returns object
   */
  public function getLanguage()
  {
    $sql="SELECT * FROM language WHERE id='{$this->id_language}' LIMIT 1";
    return $this->getObject($sql,'Language');
  }

  /**
   * User - referenced table
   * @returns object
   */
  public function getUser()
  {
    $sql="SELECT * FROM user WHERE id='{$this->id_user}' LIMIT 1";
    return $this->getObject($sql,'User');
  }

  /**
   * Packages - referred table
   * @returns object[]
   */
  public function getPackages()
  {
    $sql="SELECT * FROM package WHERE destination_id='{$this->id}'";
    return $this->getObjects($sql,'Package');
  }

  /**
   * Column city_id Finder
   * @return object[]
   */
  public function findByCityId($city_id)
  {
    $sql="SELECT * FROM destination WHERE city_id='$city_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_language Finder
   * @return object[]
   */
  public function findByIdLanguage($id_language)
  {
    $sql="SELECT * FROM destination WHERE id_language='$id_language'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_user Finder
   * @return object[]
   */
  public function findByIdUser($id_user)
  {
    $sql="SELECT * FROM destination WHERE id_user='$id_user'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM destination WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column firstname Finder
   * @return object[]
   */
  public function findByFirstname($firstname)
  {
    $sql="SELECT * FROM destination WHERE firstname='$firstname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column lastname Finder
   * @return object[]
   */
  public function findByLastname($lastname)
  {
    $sql="SELECT * FROM destination WHERE lastname='$lastname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column address Finder
   * @return object[]
   */
  public function findByAddress($address)
  {
    $sql="SELECT * FROM destination WHERE address='$address'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

