<?php

require_once 'DAO.php';

/**
 * Class PackageDAO
*/
abstract class PackageDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->customer.id
   * @var int $customer_id
   */
  protected $customer_id;

  public function getCustomerId() {return $this->customer_id;}

  public function setCustomerId($customer_id) {$this->customer_id=$customer_id;}

  /**
   * Protected variable
   * (FK)->destination.id
   * @var int $destination_id
   */
  protected $destination_id;

  public function getDestinationId() {return $this->destination_id;}

  public function setDestinationId($destination_id) {$this->destination_id=$destination_id;}

  /**
   * Protected variable
   * (FK)->dimension.id
   * @var int $dimension_id
   */
  protected $dimension_id;

  public function getDimensionId() {return $this->dimension_id;}

  public function setDimensionId($dimension_id) {$this->dimension_id=$dimension_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $barre_code
   */
  protected $barre_code;

  public function getBarreCode() {return $this->barre_code;}

  public function setBarreCode($barre_code) {$this->barre_code=$barre_code;}

  /**
   * Protected variable
   * @var varchar $name
   */
  protected $name;

  public function getName() {return $this->name;}

  public function setName($name) {$this->name=$name;}

  /**
   * Protected variable
   * @var varchar $description
   */
  protected $description;

  public function getDescription() {return $this->description;}

  public function setDescription($description) {$this->description=$description;}

  /**
   * Protected variable
   * @var tinyint $fragile
   */
  protected $fragile;

  public function getFragile() {return $this->fragile;}

  public function setFragile($fragile) {$this->fragile=$fragile;}

  /**
   * Protected variable
   * @var int $weight
   */
  protected $weight;

  public function getWeight() {return $this->weight;}

  public function setWeight($weight) {$this->weight=$weight;}

  /**
   * Protected variable
   * @var varchar $state
   */
  protected $state;

  public function getState() {return $this->state;}

  public function setState($state) {$this->state=$state;}

  /**
   * Protected variable
   * @var int $price
   */
  protected $price;

  public function getPrice() {return $this->price;}

  public function setPrice($price) {$this->price=$price;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='package';
    $this->primkeys=['id'];
    $this->fields=['customer_id','destination_id','dimension_id','barre_code','name','description','fragile','weight','state','price'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Customer - referenced table
   * @returns object
   */
  public function getCustomer()
  {
    $sql="SELECT * FROM customer WHERE id='{$this->customer_id}' LIMIT 1";
    return $this->getObject($sql,'Customer');
  }

  /**
   * Destination - referenced table
   * @returns object
   */
  public function getDestination()
  {
    $sql="SELECT * FROM destination WHERE id='{$this->destination_id}' LIMIT 1";
    return $this->getObject($sql,'Destination');
  }

  /**
   * Dimension - referenced table
   * @returns object
   */
  public function getDimension()
  {
    $sql="SELECT * FROM dimension WHERE id='{$this->dimension_id}' LIMIT 1";
    return $this->getObject($sql,'Dimension');
  }

  /**
   * Localisations - referred table
   * @returns object[]
   */
  public function getLocalisations()
  {
    $sql="SELECT * FROM localisation WHERE package_id='{$this->id}'";
    return $this->getObjects($sql,'Localisation');
  }

  /**
   * Reclamations - referred table
   * @returns object[]
   */
  public function getReclamations()
  {
    $sql="SELECT * FROM reclamation WHERE package_id='{$this->id}'";
    return $this->getObjects($sql,'Reclamation');
  }

  /**
   * Column customer_id Finder
   * @return object[]
   */
  public function findByCustomerId($customer_id)
  {
    $sql="SELECT * FROM package WHERE customer_id='$customer_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column destination_id Finder
   * @return object[]
   */
  public function findByDestinationId($destination_id)
  {
    $sql="SELECT * FROM package WHERE destination_id='$destination_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column dimension_id Finder
   * @return object[]
   */
  public function findByDimensionId($dimension_id)
  {
    $sql="SELECT * FROM package WHERE dimension_id='$dimension_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM package WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByBarreCode($barre_code)
  {
    $sql="SELECT * FROM package WHERE barre_code='$barre_code' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column name Finder
   * @return object[]
   */
  public function findByName($name)
  {
    $sql="SELECT * FROM package WHERE name='$name'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column description Finder
   * @return object[]
   */
  public function findByDescription($description)
  {
    $sql="SELECT * FROM package WHERE description='$description'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column fragile Finder
   * @return object[]
   */
  public function findByFragile($fragile)
  {
    $sql="SELECT * FROM package WHERE fragile='$fragile'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column weight Finder
   * @return object[]
   */
  public function findByWeight($weight)
  {
    $sql="SELECT * FROM package WHERE weight='$weight'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column state Finder
   * @return object[]
   */
  public function findByState($state)
  {
    $sql="SELECT * FROM package WHERE state='$state'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column price Finder
   * @return object[]
   */
  public function findByPrice($price)
  {
    $sql="SELECT * FROM package WHERE price='$price'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

