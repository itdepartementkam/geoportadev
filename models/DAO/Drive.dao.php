<?php

require_once 'DAO.php';

/**
 * Class DriveDAO
*/
abstract class DriveDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->transporter.id
   * @var int $transporter_id
   */
  protected $transporter_id;

  public function getTransporterId() {return $this->transporter_id;}

  public function setTransporterId($transporter_id) {$this->transporter_id=$transporter_id;}

  /**
   * Protected variable
   * (FK)->vehicle.id
   * @var int $vehicle_id
   */
  protected $vehicle_id;

  public function getVehicleId() {return $this->vehicle_id;}

  public function setVehicleId($vehicle_id) {$this->vehicle_id=$vehicle_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * @var timestamp $create_time
   */
  protected $create_time;

  public function getCreateTime() {return $this->create_time;}

  public function setCreateTime($create_time) {$this->create_time=$create_time;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='drive';
    $this->primkeys=['id'];
    $this->fields=['transporter_id','vehicle_id','create_time'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Transporter - referenced table
   * @returns object
   */
  public function getTransporter()
  {
    $sql="SELECT * FROM transporter WHERE id='{$this->transporter_id}' LIMIT 1";
    return $this->getObject($sql,'Transporter');
  }

  /**
   * Vehicle - referenced table
   * @returns object
   */
  public function getVehicle()
  {
    $sql="SELECT * FROM vehicle WHERE id='{$this->vehicle_id}' LIMIT 1";
    return $this->getObject($sql,'Vehicle');
  }

  /**
   * Column transporter_id Finder
   * @return object[]
   */
  public function findByTransporterId($transporter_id)
  {
    $sql="SELECT * FROM drive WHERE transporter_id='$transporter_id'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column vehicle_id Finder
   * @return object[]
   */
  public function findByVehicleId($vehicle_id)
  {
    $sql="SELECT * FROM drive WHERE vehicle_id='$vehicle_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM drive WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column create_time Finder
   * @return object[]
   */
  public function findByCreateTime($create_time)
  {
    $sql="SELECT * FROM drive WHERE create_time='$create_time'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

