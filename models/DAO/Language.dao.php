<?php

require_once 'DAO.php';

/**
 * Class LanguageDAO
*/
abstract class LanguageDAO extends EntityBase
{

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $name
   */
  protected $name;

  public function getName() {return $this->name;}

  public function setName($name) {$this->name=$name;}

  /**
   * Protected variable
   * @var varchar $suffixe
   */
  protected $suffixe;

  public function getSuffixe() {return $this->suffixe;}

  public function setSuffixe($suffixe) {$this->suffixe=$suffixe;}

  /**
   * Protected variable
   * @var timestamp $create_time
   */
  protected $create_time;

  public function getCreateTime() {return $this->create_time;}

  public function setCreateTime($create_time) {$this->create_time=$create_time;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='language';
    $this->primkeys=['id'];
    $this->fields=['name','suffixe','create_time'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Admins - referred table
   * @returns object[]
   */
  public function getAdmins()
  {
    $sql="SELECT * FROM admin WHERE id_languge='{$this->id}'";
    return $this->getObjects($sql,'Admin');
  }

  /**
   * Customers - referred table
   * @returns object[]
   */
  public function getCustomers()
  {
    $sql="SELECT * FROM customer WHERE id_languge='{$this->id}'";
    return $this->getObjects($sql,'Customer');
  }

  /**
   * Destinations - referred table
   * @returns object[]
   */
  public function getDestinations()
  {
    $sql="SELECT * FROM destination WHERE id_language='{$this->id}'";
    return $this->getObjects($sql,'Destination');
  }

  /**
   * Transporters - referred table
   * @returns object[]
   */
  public function getTransporters()
  {
    $sql="SELECT * FROM transporter WHERE id_language='{$this->id}'";
    return $this->getObjects($sql,'Transporter');
  }

  /**
   * Warehousemanegers - referred table
   * @returns object[]
   */
  public function getWarehousemanegers()
  {
    $sql="SELECT * FROM warehousemaneger WHERE id_languge='{$this->id}'";
    return $this->getObjects($sql,'Warehousemaneger');
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM language WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByName($name)
  {
    $sql="SELECT * FROM language WHERE name='$name' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column suffixe Finder
   * @return object[]
   */
  public function findBySuffixe($suffixe)
  {
    $sql="SELECT * FROM language WHERE suffixe='$suffixe'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column create_time Finder
   * @return object[]
   */
  public function findByCreateTime($create_time)
  {
    $sql="SELECT * FROM language WHERE create_time='$create_time'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

