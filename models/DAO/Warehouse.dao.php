<?php

require_once 'DAO.php';

/**
 * Class WarehouseDAO
*/
abstract class WarehouseDAO extends EntityBase
{

  /**
   * Protected variable
   * (UQ)->Unique key
   * (FK)->warehousemaneger.id
   * @var int $id_warehousemanger
   */
  protected $id_warehousemanger;

  public function getIdWarehousemanger() {return $this->id_warehousemanger;}

  public function setIdWarehousemanger($id_warehousemanger) {$this->id_warehousemanger=$id_warehousemanger;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * @var varchar $name
   */
  protected $name;

  public function getName() {return $this->name;}

  public function setName($name) {$this->name=$name;}

  /**
   * Protected variable
   * @var varchar $address
   */
  protected $address;

  public function getAddress() {return $this->address;}

  public function setAddress($address) {$this->address=$address;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='warehouse';
    $this->primkeys=['id'];
    $this->fields=['id_warehousemanger','name','address'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Warehousemaneger - referenced table
   * @returns object
   */
  public function getWarehousemaneger()
  {
    $sql="SELECT * FROM warehousemaneger WHERE id='{$this->id_warehousemanger}' LIMIT 1";
    return $this->getObject($sql,'Warehousemaneger');
  }

  /**
   * Cities - referred table
   * @returns object[]
   */
  public function getCities()
  {
    $sql="SELECT * FROM city WHERE warehouse_id='{$this->id}'";
    return $this->getObjects($sql,'City');
  }

  /**
   * Localisations - referred table
   * @returns object[]
   */
  public function getLocalisations()
  {
    $sql="SELECT * FROM localisation WHERE warehouse_id='{$this->id}'";
    return $this->getObjects($sql,'Localisation');
  }

  /**
   * Vehicles - referred table
   * @returns object[]
   */
  public function getVehicles()
  {
    $sql="SELECT * FROM vehicle WHERE warehouse_id='{$this->id}'";
    return $this->getObjects($sql,'Vehicle');
  }

  /**
   * Warehousemanegers - referred table
   * @returns object[]
   */
  public function getWarehousemanegers()
  {
    $sql="SELECT * FROM warehousemaneger WHERE id_warehouse='{$this->id}'";
    return $this->getObjects($sql,'Warehousemaneger');
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByIdWarehousemanger($id_warehousemanger)
  {
    $sql="SELECT * FROM warehouse WHERE id_warehousemanger='$id_warehousemanger' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM warehouse WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column name Finder
   * @return object[]
   */
  public function findByName($name)
  {
    $sql="SELECT * FROM warehouse WHERE name='$name'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column address Finder
   * @return object[]
   */
  public function findByAddress($address)
  {
    $sql="SELECT * FROM warehouse WHERE address='$address'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

