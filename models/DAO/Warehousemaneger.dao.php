<?php

require_once 'DAO.php';

/**
 * Class WarehousemanegerDAO
*/
abstract class WarehousemanegerDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->city.id
   * @var int $id_city
   */
  protected $id_city;

  public function getIdCity() {return $this->id_city;}

  public function setIdCity($id_city) {$this->id_city=$id_city;}

  /**
   * Protected variable
   * (FK)->language.id
   * @var int $id_languge
   */
  protected $id_languge;

  public function getIdLanguge() {return $this->id_languge;}

  public function setIdLanguge($id_languge) {$this->id_languge=$id_languge;}

  /**
   * Protected variable
   * (FK)->user.id
   * @var int $id_user
   */
  protected $id_user;

  public function getIdUser() {return $this->id_user;}

  public function setIdUser($id_user) {$this->id_user=$id_user;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * (FK)->warehouse.id
   * @var int $id_warehouse
   */
  protected $id_warehouse;

  public function getIdWarehouse() {return $this->id_warehouse;}

  public function setIdWarehouse($id_warehouse) {$this->id_warehouse=$id_warehouse;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * @var varchar $firstname
   */
  protected $firstname;

  public function getFirstname() {return $this->firstname;}

  public function setFirstname($firstname) {$this->firstname=$firstname;}

  /**
   * Protected variable
   * @var varchar $lastname
   */
  protected $lastname;

  public function getLastname() {return $this->lastname;}

  public function setLastname($lastname) {$this->lastname=$lastname;}

  /**
   * Protected variable
   * @var varchar $address
   */
  protected $address;

  public function getAddress() {return $this->address;}

  public function setAddress($address) {$this->address=$address;}

  /**
   * Protected variable
   * @var varchar $phone1
   */
  protected $phone1;

  public function getPhone1() {return $this->phone1;}

  public function setPhone1($phone1) {$this->phone1=$phone1;}

  /**
   * Protected variable
   * @var varchar $phone2
   */
  protected $phone2;

  public function getPhone2() {return $this->phone2;}

  public function setPhone2($phone2) {$this->phone2=$phone2;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='warehousemaneger';
    $this->primkeys=['id'];
    $this->fields=['id_city','id_languge','id_user','id_warehouse','firstname','lastname','address','phone1','phone2'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * City - referenced table
   * @returns object
   */
  public function getCity()
  {
    $sql="SELECT * FROM city WHERE id='{$this->id_city}' LIMIT 1";
    return $this->getObject($sql,'City');
  }

  /**
   * Language - referenced table
   * @returns object
   */
  public function getLanguage()
  {
    $sql="SELECT * FROM language WHERE id='{$this->id_languge}' LIMIT 1";
    return $this->getObject($sql,'Language');
  }

  /**
   * User - referenced table
   * @returns object
   */
  public function getUser()
  {
    $sql="SELECT * FROM user WHERE id='{$this->id_user}' LIMIT 1";
    return $this->getObject($sql,'User');
  }

  /**
   * Warehouse - referenced table
   * @returns object
   */
  public function getWarehouse()
  {
    $sql="SELECT * FROM warehouse WHERE id='{$this->id_warehouse}' LIMIT 1";
    return $this->getObject($sql,'Warehouse');
  }

  /**
   * Warehouses - referred table
   * @returns object[]
   */
  public function getWarehouses()
  {
    $sql="SELECT * FROM warehouse WHERE id_warehousemanger='{$this->id}'";
    return $this->getObjects($sql,'Warehouse');
  }

  /**
   * Column id_city Finder
   * @return object[]
   */
  public function findByIdCity($id_city)
  {
    $sql="SELECT * FROM warehousemaneger WHERE id_city='$id_city'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_languge Finder
   * @return object[]
   */
  public function findByIdLanguge($id_languge)
  {
    $sql="SELECT * FROM warehousemaneger WHERE id_languge='$id_languge'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_user Finder
   * @return object[]
   */
  public function findByIdUser($id_user)
  {
    $sql="SELECT * FROM warehousemaneger WHERE id_user='$id_user'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByIdWarehouse($id_warehouse)
  {
    $sql="SELECT * FROM warehousemaneger WHERE id_warehouse='$id_warehouse' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM warehousemaneger WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column firstname Finder
   * @return object[]
   */
  public function findByFirstname($firstname)
  {
    $sql="SELECT * FROM warehousemaneger WHERE firstname='$firstname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column lastname Finder
   * @return object[]
   */
  public function findByLastname($lastname)
  {
    $sql="SELECT * FROM warehousemaneger WHERE lastname='$lastname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column address Finder
   * @return object[]
   */
  public function findByAddress($address)
  {
    $sql="SELECT * FROM warehousemaneger WHERE address='$address'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column phone1 Finder
   * @return object[]
   */
  public function findByPhone1($phone1)
  {
    $sql="SELECT * FROM warehousemaneger WHERE phone1='$phone1'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column phone2 Finder
   * @return object[]
   */
  public function findByPhone2($phone2)
  {
    $sql="SELECT * FROM warehousemaneger WHERE phone2='$phone2'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

