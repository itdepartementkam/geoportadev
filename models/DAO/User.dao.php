<?php

require_once 'DAO.php';

/**
 * Class UserDAO
*/
abstract class UserDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->role.id
   * @var int $role_id
   */
  protected $role_id;

  public function getRoleId() {return $this->role_id;}

  public function setRoleId($role_id) {$this->role_id=$role_id;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $email
   */
  protected $email;

  public function getEmail() {return $this->email;}

  public function setEmail($email) {$this->email=$email;}

  /**
   * Protected variable
   * @var varchar $password
   */
  protected $password;

  public function getPassword() {return $this->password;}

  public function setPassword($password) {$this->password=$password;}

  /**
   * Protected variable
   * @var timestamp $create_time
   */
  protected $create_time;

  public function getCreateTime() {return $this->create_time;}

  public function setCreateTime($create_time) {$this->create_time=$create_time;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='user';
    $this->primkeys=['id'];
    $this->fields=['role_id','email','password','create_time'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Role - referenced table
   * @returns object
   */
  public function getRole()
  {
    $sql="SELECT * FROM role WHERE id='{$this->role_id}' LIMIT 1";
    return $this->getObject($sql,'Role');
  }

  /**
   * Admins - referred table
   * @returns object[]
   */
  public function getAdmins()
  {
    $sql="SELECT * FROM admin WHERE id_user='{$this->id}'";
    return $this->getObjects($sql,'Admin');
  }

  /**
   * Customers - referred table
   * @returns object[]
   */
  public function getCustomers()
  {
    $sql="SELECT * FROM customer WHERE user_id='{$this->id}'";
    return $this->getObjects($sql,'Customer');
  }

  /**
   * Destinations - referred table
   * @returns object[]
   */
  public function getDestinations()
  {
    $sql="SELECT * FROM destination WHERE id_user='{$this->id}'";
    return $this->getObjects($sql,'Destination');
  }

  /**
   * Transporters - referred table
   * @returns object[]
   */
  public function getTransporters()
  {
    $sql="SELECT * FROM transporter WHERE user_id='{$this->id}'";
    return $this->getObjects($sql,'Transporter');
  }

  /**
   * Warehousemanegers - referred table
   * @returns object[]
   */
  public function getWarehousemanegers()
  {
    $sql="SELECT * FROM warehousemaneger WHERE id_user='{$this->id}'";
    return $this->getObjects($sql,'Warehousemaneger');
  }

  /**
   * Column role_id Finder
   * @return object[]
   */
  public function findByRoleId($role_id)
  {
    $sql="SELECT * FROM user WHERE role_id='$role_id'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM user WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByEmail($email)
  {
    $sql="SELECT * FROM user WHERE email='$email' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column password Finder
   * @return object[]
   */
  public function findByPassword($password)
  {
    $sql="SELECT * FROM user WHERE password='$password'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column create_time Finder
   * @return object[]
   */
  public function findByCreateTime($create_time)
  {
    $sql="SELECT * FROM user WHERE create_time='$create_time'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

