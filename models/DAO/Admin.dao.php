<?php

require_once 'DAO.php';

/**
 * Class AdminDAO
*/
abstract class AdminDAO extends EntityBase
{

  /**
   * Protected variable
   * (FK)->city.id
   * @var int $id_city
   */
  protected $id_city;

  public function getIdCity() {return $this->id_city;}

  public function setIdCity($id_city) {$this->id_city=$id_city;}

  /**
   * Protected variable
   * (FK)->customer.id
   * @var int $id_client
   */
  protected $id_client;

  public function getIdClient() {return $this->id_client;}

  public function setIdClient($id_client) {$this->id_client=$id_client;}

  /**
   * Protected variable
   * (FK)->language.id
   * @var int $id_languge
   */
  protected $id_languge;

  public function getIdLanguge() {return $this->id_languge;}

  public function setIdLanguge($id_languge) {$this->id_languge=$id_languge;}

  /**
   * Protected variable
   * (FK)->user.id
   * @var int $id_user
   */
  protected $id_user;

  public function getIdUser() {return $this->id_user;}

  public function setIdUser($id_user) {$this->id_user=$id_user;}

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * @var varchar $firstname
   */
  protected $firstname;

  public function getFirstname() {return $this->firstname;}

  public function setFirstname($firstname) {$this->firstname=$firstname;}

  /**
   * Protected variable
   * @var varchar $lastname
   */
  protected $lastname;

  public function getLastname() {return $this->lastname;}

  public function setLastname($lastname) {$this->lastname=$lastname;}

  /**
   * Protected variable
   * @var varchar $address
   */
  protected $address;

  public function getAddress() {return $this->address;}

  public function setAddress($address) {$this->address=$address;}

  /**
   * Protected variable
   * @var varchar $phone1
   */
  protected $phone1;

  public function getPhone1() {return $this->phone1;}

  public function setPhone1($phone1) {$this->phone1=$phone1;}

  /**
   * Protected variable
   * @var varchar $phone2
   */
  protected $phone2;

  public function getPhone2() {return $this->phone2;}

  public function setPhone2($phone2) {$this->phone2=$phone2;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='admin';
    $this->primkeys=['id'];
    $this->fields=['id_city','id_client','id_languge','id_user','firstname','lastname','address','phone1','phone2'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * City - referenced table
   * @returns object
   */
  public function getCity()
  {
    $sql="SELECT * FROM city WHERE id='{$this->id_city}' LIMIT 1";
    return $this->getObject($sql,'City');
  }

  /**
   * Customer - referenced table
   * @returns object
   */
  public function getCustomer()
  {
    $sql="SELECT * FROM customer WHERE id='{$this->id_client}' LIMIT 1";
    return $this->getObject($sql,'Customer');
  }

  /**
   * Language - referenced table
   * @returns object
   */
  public function getLanguage()
  {
    $sql="SELECT * FROM language WHERE id='{$this->id_languge}' LIMIT 1";
    return $this->getObject($sql,'Language');
  }

  /**
   * User - referenced table
   * @returns object
   */
  public function getUser()
  {
    $sql="SELECT * FROM user WHERE id='{$this->id_user}' LIMIT 1";
    return $this->getObject($sql,'User');
  }

  /**
   * Column id_city Finder
   * @return object[]
   */
  public function findByIdCity($id_city)
  {
    $sql="SELECT * FROM admin WHERE id_city='$id_city'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_client Finder
   * @return object[]
   */
  public function findByIdClient($id_client)
  {
    $sql="SELECT * FROM admin WHERE id_client='$id_client'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_languge Finder
   * @return object[]
   */
  public function findByIdLanguge($id_languge)
  {
    $sql="SELECT * FROM admin WHERE id_languge='$id_languge'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column id_user Finder
   * @return object[]
   */
  public function findByIdUser($id_user)
  {
    $sql="SELECT * FROM admin WHERE id_user='$id_user'";
    return $this->getSelfObjects($sql);
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM admin WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column firstname Finder
   * @return object[]
   */
  public function findByFirstname($firstname)
  {
    $sql="SELECT * FROM admin WHERE firstname='$firstname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column lastname Finder
   * @return object[]
   */
  public function findByLastname($lastname)
  {
    $sql="SELECT * FROM admin WHERE lastname='$lastname'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column address Finder
   * @return object[]
   */
  public function findByAddress($address)
  {
    $sql="SELECT * FROM admin WHERE address='$address'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column phone1 Finder
   * @return object[]
   */
  public function findByPhone1($phone1)
  {
    $sql="SELECT * FROM admin WHERE phone1='$phone1'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column phone2 Finder
   * @return object[]
   */
  public function findByPhone2($phone2)
  {
    $sql="SELECT * FROM admin WHERE phone2='$phone2'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

