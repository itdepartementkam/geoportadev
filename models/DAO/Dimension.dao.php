<?php

require_once 'DAO.php';

/**
 * Class DimensionDAO
*/
abstract class DimensionDAO extends EntityBase
{

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $name
   */
  protected $name;

  public function getName() {return $this->name;}

  public function setName($name) {$this->name=$name;}

  /**
   * Protected variable
   * @var int $height
   */
  protected $height;

  public function getHeight() {return $this->height;}

  public function setHeight($height) {$this->height=$height;}

  /**
   * Protected variable
   * @var int $width
   */
  protected $width;

  public function getWidth() {return $this->width;}

  public function setWidth($width) {$this->width=$width;}

  /**
   * Protected variable
   * @var int $length
   */
  protected $length;

  public function getLength() {return $this->length;}

  public function setLength($length) {$this->length=$length;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='dimension';
    $this->primkeys=['id'];
    $this->fields=['name','height','width','length'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Packages - referred table
   * @returns object[]
   */
  public function getPackages()
  {
    $sql="SELECT * FROM package WHERE dimension_id='{$this->id}'";
    return $this->getObjects($sql,'Package');
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM dimension WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByName($name)
  {
    $sql="SELECT * FROM dimension WHERE name='$name' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column height Finder
   * @return object[]
   */
  public function findByHeight($height)
  {
    $sql="SELECT * FROM dimension WHERE height='$height'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column width Finder
   * @return object[]
   */
  public function findByWidth($width)
  {
    $sql="SELECT * FROM dimension WHERE width='$width'";
    return $this->getSelfObjects($sql);
  }

  /**
   * Column length Finder
   * @return object[]
   */
  public function findByLength($length)
  {
    $sql="SELECT * FROM dimension WHERE length='$length'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

