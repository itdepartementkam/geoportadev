<?php

require_once 'DAO.php';

/**
 * Class CountryDAO
*/
abstract class CountryDAO extends EntityBase
{

  /**
   * Protected variable
   * (PK)->Primary key
   * @var int $id
   */
  protected $id;

  public function getId() {return $this->id;}

  public function setId($id) {$this->id=$id;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $name
   */
  protected $name;

  public function getName() {return $this->name;}

  public function setName($name) {$this->name=$name;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $barre_code
   */
  protected $barre_code;

  public function getBarreCode() {return $this->barre_code;}

  public function setBarreCode($barre_code) {$this->barre_code=$barre_code;}

  /**
   * Protected variable
   * (UQ)->Unique key
   * @var varchar $phone_code
   */
  protected $phone_code;

  public function getPhoneCode() {return $this->phone_code;}

  public function setPhoneCode($phone_code) {$this->phone_code=$phone_code;}

  /**
   * Protected variable
   * @var timestamp $create_time
   */
  protected $create_time;

  public function getCreateTime() {return $this->create_time;}

  public function setCreateTime($create_time) {$this->create_time=$create_time;}

  /**
   * Constructor
   * @var mixed $id
   */
  public function __construct($id=0)
  {
    parent::__construct();
    $this->table='country';
    $this->primkeys=['id'];
    $this->fields=['name','barre_code','phone_code','create_time'];
    $this->sql="SELECT * FROM {$this->table}";
    if($id) $this->read($id);
  }

  /**
   * Cities - referred table
   * @returns object[]
   */
  public function getCities()
  {
    $sql="SELECT * FROM city WHERE country_id='{$this->id}'";
    return $this->getObjects($sql,'City');
  }

  /**
  /* Primary Key Finder
   * @return object
   */
  public function findById($id)
  {
    $sql="SELECT * FROM country WHERE id='$id' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByName($name)
  {
    $sql="SELECT * FROM country WHERE name='$name' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByBarreCode($barre_code)
  {
    $sql="SELECT * FROM country WHERE barre_code='$barre_code' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
  /* Unique Key Finder
   * @return object
   */
  public function findByPhoneCode($phone_code)
  {
    $sql="SELECT * FROM country WHERE phone_code='$phone_code' LIMIT 1";
    return $this->getSelfObject($sql);
  }

  /**
   * Column create_time Finder
   * @return object[]
   */
  public function findByCreateTime($create_time)
  {
    $sql="SELECT * FROM country WHERE create_time='$create_time'";
    return $this->getSelfObjects($sql);
  }

  // ==========!!!DO NOT PUT YOUR OWN CODE (BUSINESS LOGIC) HERE!!!========== //
  // EXTEND THIS DAO CLASS WITH YOUR OWN CLASS CONTAINING YOUR BUSINESS LOGIC //
  //  BECAUSE THIS CLASS FILE WILL BE OVERWRITTEN UPON EACH NEW PHPDAO BUILD  //
  // ======================================================================== //
}

