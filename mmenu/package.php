<?php include_once("includes/verif-lang.php") ?>
<!DOCTYPE html>
<html class = 'no-js css-menubar' lang = 'en'>

<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:33:04 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Dashboard </title>

  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="../global/css/bootstrap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/css/bootstrap-extend.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="assets/css/site.minfd53.css?v4.0.1">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="../global/css/skintools.minfd53.css?v4.0.1">
  <script src="assets/js/Plugin/skintools.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="../global/vendor/animsition/animsition.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/asscrollable/asScrollable.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/switchery/switchery.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/intro-js/introjs.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/slidepanel/slidePanel.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jquery-mmenu/jquery-mmenu.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/flag-icon-css/flag-icon.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/waves/waves.minfd53.css?v4.0.1">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="../global/vendor/chartist/chartist.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jvectormap/jquery-jvectormap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.css?v4.0.1">

  <!-- Page -->
  <link rel="stylesheet" href="assets/examples/css/dashboard/v1.minfd53.css?v4.0.1">

  <!-- Fonts -->
  <link rel="stylesheet" href="../global/fonts/material-design/material-design.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/fonts/brand-icons/brand-icons.minfd53.css?v4.0.1">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">


  <!-- Scripts -->
  <script src="../global/vendor/breakpoints/breakpoints.minfd53.js?v4.0.1"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="animsition site-navbar-small dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
<?php

require('../../models/Package.php');
require('../../models/Destination.php');
require('../../models/Customer.php');
$Package=new Package();
$customer=new Customer();
$destination=new Destination();
?>
 <?php include 'includes/navbar.php';?>
 <?php include 'includes/menu.php';?>
 <?php include 'en.php' ?>
 <?php include 'de.php' ?>


  <!-- Page -->
  <div class="page">



        <!-- Panel Table Add Row -->
        <div class="panel">
        <header class="panel-heading">
          <h3 class="panel-title"><?php echo $ListePackages; ?></h3>
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
              <div class="mb-15">
                <button id="addToTable" class="btn btn-primary" type="button">
                  <i class="icon md-plus" aria-hidden="true"></i> <?php echo $Addpackage; ?>
                </button>
              </div>
            </div>
          </div>
          <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
            <thead>
              <tr>
              <th><?php echo $Customer; ?></th>
              <th><?php echo $Destination; ?></th>
                <th><?php echo $Namepackage; ?></th>
                <th><?php echo $Fragile; ?></th>
                <th><?php echo $Weight; ?></th>
                <th><?php echo $Price; ?></th>
                <th><?php echo $BarreCode; ?></th>
                <th><?php echo $Actions; ?></th>

              </tr>
            </thead>
            <tbody>

            <?php foreach ($Package->getAll() as $p) {  ?>
            <tr>
            <td ><?=$p->getCustomer()->getFirstname(); ?> <?= $p->getCustomer()->getLastname(); ?></td>
            <td><?= $p->getDestination()->getFirstname(); ?><?= $p->getDestination()->getLastname(); ?></td>
            <td><?= $p->getName(); ?></td>
            <td><?= $p->getFragile(); ?></td>
            <td><?= $p->getWeight(); ?></td>
            <td><?= $p->getPrice(); ?></td>
            <td><?= $p->getBarreCode(); ?></td>
            <?php $pack=$p->getId() ?>
            <td>
            <a href="updatePackage.php?edit_id=<?= $pack ?>" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                    data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i>update</a></td>
             </tr>
              <?php } ?>

            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel Table Add Row -->
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <?php include 'includes/footer.php';?>
  <!-- Core  -->
  <script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="../global/vendor/babel-external-helpers/babel-external-helpersfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jquery/jquery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/popper-js/umd/popper.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/bootstrap/bootstrap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/animsition/animsition.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/mousewheel/jquery.mousewheel.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollbar/jquery-asScrollbar.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollable/jquery-asScrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/waves/waves.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <script src="../global/vendor/jquery-mmenu/jquery.mmenu.min.allfd53.js?v4.0.1"></script>
  <script src="../global/vendor/switchery/switchery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/intro-js/intro.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/screenfull/screenfull.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/slidepanel/jquery-slidePanel.minfd53.js?v4.0.1"></script>

  <!-- Plugins For This Page -->
  <script src="../global/vendor/chartist/chartist.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/jquery-jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-enfd53.js?v4.0.1"></script>
  <script src="../global/vendor/matchheight/jquery.matchHeight-minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/peity/jquery.peity.minfd53.js?v4.0.1"></script>

  <!-- Scripts -->
  <script src="../global/js/Component.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Base.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Config.minfd53.js?v4.0.1"></script>

  <script src="assets/js/Section/Menubar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/Sidebar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/PageAside.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/GridMenu.minfd53.js?v4.0.1"></script>
  <!-- Config -->
  <script src="../global/js/config/colors.minfd53.js?v4.0.1"></script>
  <script src="assets/js/config/tour.minfd53.js?v4.0.1"></script>
  <script>
    Config.set('assets', 'assets');
  </script>

  <!-- Page -->
  <script src="assets/js/Site.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/asscrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/slidepanel.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/switchery.minfd53.js?v4.0.1"></script>

  <script src="../global/js/Plugin/matchheight.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/peity.minfd53.js?v4.0.1"></script>


  <script src="assets/examples/js/dashboard/v1.minfd53.js?v4.0.1"></script>

</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:34:21 GMT -->
</html>