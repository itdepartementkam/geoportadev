 <div class="site-menubar">
    <ul class="site-menu">
      <li class="site-menu-item active">
        <a href="index.php">
                  <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                  <span class="site-menu-title">index</span>
              </a>
      </li>
      <li class="site-menu-item active">
        <a href="package.php">
                  <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                  <span class="site-menu-title">Packages</span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="dimension.php">
                  <i class="site-menu-icon md-view-compact" aria-hidden="true"></i>
                  <span class="site-menu-title">Dimension</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="drive.php">
                  <i class="site-menu-icon md-google-pages" aria-hidden="true"></i>
                  <span class="site-menu-title">Driver</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="Warehouse.php">
                  <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                  <span class="site-menu-title">Warehouse</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="Town.php">
                  <i class="site-menu-icon md-format-color-fill" aria-hidden="true"></i>
                  <span class="site-menu-title">Town</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="Localisation.php">
                  <i class="site-menu-icon md-puzzle-piece" aria-hidden="true"></i>
                  <span class="site-menu-title">Localisation</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="Language.php">
                  <i class="site-menu-icon md-widgets" aria-hidden="true"></i>
                  <span class="site-menu-title">Language</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="Country.php">
                  <i class="site-menu-icon md-comment-alt-text" aria-hidden="true"></i>
                  <span class="site-menu-title">Country</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="User.php">
                  <i class="site-menu-icon md-border-all" aria-hidden="true"></i>
                  <span class="site-menu-title">User</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="reclamation.php">
                  <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                  <span class="site-menu-title">Reclamation</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="destination.php">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Destination</span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="Transporter.php">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">livreur
                  </span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="client.php">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Client
                  </span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="vehicule.php">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">Vehicule
                  </span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
      <li class="site-menu-item has-sub">
        <a href="WarehouseManager.php">
                  <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                  <span class="site-menu-title">WareHouseManager
                  </span>
                          <span class="site-menu-arrow"></span>
              </a>
      </li>
    </ul>
  </div>
  <div class="site-gridmenu">
    <div>
      <div>
        <ul>
          <li>
            <a href="#">
                <i class="icon md-email"></i>
                <span>Mailbox</span>
              </a>
          </li>
          <li>
            <a href="#">
                <i class="icon md-calendar"></i>
                <span>Calendar</span>
              </a>
          </li>
          <li>
            <a href="#">
                <i class="icon md-account"></i>
                <span>Contacts</span>
              </a>
          </li>
          <li>
            <a href="#">
                <i class="icon md-videocam"></i>
                <span>Media</span>
              </a>
          </li>
          <li>
            <a href="#">
                <i class="icon md-receipt"></i>
                <span>Documents</span>
              </a>
          </li>
          <li>
            <a href="#">
                <i class="icon md-image"></i>
                <span>Project</span>
              </a>
          </li>
          <li>
            <a href="#">
                <i class="icon md-comments"></i>
                <span>Forum</span>
              </a>
          </li>
          <li>
            <a href="index.php">
                <i class="icon md-view-dashboard"></i>
                <span>Dashboard</span>
              </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
