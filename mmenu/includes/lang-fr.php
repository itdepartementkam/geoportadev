<?php
//page client
define('FirstName','Nom');
define('LastName','Prenom');
define('Address','Addresse');
define('Phone1','Mobile 1');
define('Phone2','Mobile 2');
define('ListCustomers','Lists Clients');
define('AddCustomer','Add Client');
define('update','Modifier');
define('Actions','Actions');

//***********************Vehicule *********** */
define('Number','Numero');
define('Brands','Marque');
define('Status','Statut');
define('Fuel','Carburant');
define('AddVehicle','Ajouter Vehicle');
define('Wheight','Poids');
define('Type','Type');
define('ListeVehicles','Liste des véhicules');
//***********************warehouse.php *****/
define('title_warehouse','Afficher tous les entrepôts');
define('show_warehouse','Listes des entrepôts');
define('name_warehouse','Npm de lentrepôt');
define('adresse','Adresse');
define('New_Warehouse','Créer un entrepôt');
define('Edit_warehouse','Editer un entrepôt');
define('Adress','Adresse');
define('Town','Ville');
define('UserEmail','EmailClient');
define('Salary','Salaire');


//***************Town.php**************/
define('title_town','Afficher tous les villes');
define('Name_town','Nom');
define('Zip_code','Zip code');
define('Country','Pays');
define('Warehouse','Entrepôt');
define('Created_at','Date de création');
define('show_city','Affciher ville');
define('Search','Rechercher');
define('Edit_City','Editer ville');
define('New_City','Nouveau ville');
define('cancel','Fermer');
//***************Package.php+dimension.php+drive.php**************/
$ListePackages = '*******';
$Addpackage = 'Ajouter Colis';
$Customer = 'Client';
$Destination = 'Destination';
$Namepackage = 'Nom du colis';
$Fragile = 'Fragile';
$Weight = 'Poids';
$Price = 'Prix';
$BarreCode = 'Code à Barre ';
$Listedrive = 'Liste des transporteurs';
$ADD = 'Ajouter';
$Actions = 'Actions';
$VehicleNumber = 'Numéro du Vehicle';
$transporterEmail = 'Email du Transporteur ';
$Width = 'Largeur';
$Create_time="Creation date";
$Listdimension = 'Liste des  Dimensions';
$Adddimension = 'Ajouter une Dimension';
$Name = 'Nom';
$height = 'Longueur';
$length = 'length';
//***************country.php+user.php+language.php**************/
$name="Name";
$description="Description";
$customer="Customer";
$barcode="Barcode";
$phone_code="Phone code";
$destination="Destination Adress";
$npackage="New Package";
$update="update";
$action="Actions";
$create_time="Creation date";
$email="email";
$role="Role";
$addCountry="Add Country";
$listeCountries="Liste Countries";
$addUser="Add User";
$listeUsers="Users List";
$language="Language";
$customer="Client";
$package="Colis";
$commentaire="Commentaire";
$status="Statut";
$Addreclamation="Ajouter une  Reclamation";
$languge="Language";
$City="Ville";
$Responsable="Responsable";
$warehouse="depot";
$ListeLanguages="Liste des Languages";
$AddLanguage="Ajouter Language"
?>
