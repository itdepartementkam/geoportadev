<?php include_once("includes/verif-lang.php") ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:33:04 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Dashboard</title>

  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="../global/css/bootstrap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/css/bootstrap-extend.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="assets/css/site.minfd53.css?v4.0.1">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="../global/css/skintools.minfd53.css?v4.0.1">
  <script src="assets/js/Plugin/skintools.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="../global/vendor/animsition/animsition.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/asscrollable/asScrollable.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/switchery/switchery.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/intro-js/introjs.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/slidepanel/slidePanel.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jquery-mmenu/jquery-mmenu.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/flag-icon-css/flag-icon.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/waves/waves.minfd53.css?v4.0.1">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="../global/vendor/chartist/chartist.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/jvectormap/jquery-jvectormap.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.css?v4.0.1">

  <!-- Page -->
  <link rel="stylesheet" href="assets/examples/css/dashboard/v1.minfd53.css?v4.0.1">

  <!-- Fonts -->
  <link rel="stylesheet" href="../global/fonts/material-design/material-design.minfd53.css?v4.0.1">
  <link rel="stylesheet" href="../global/fonts/brand-icons/brand-icons.minfd53.css?v4.0.1">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">


  <!-- Scripts -->
  <script src="../global/vendor/breakpoints/breakpoints.minfd53.js?v4.0.1"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="animsition site-navbar-small dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    
<?php
 require('../../models/Language.php');
$Language=new Language(); 
?>
 <?php include 'includes/navbar.php';?>
 <?php include 'includes/menu.php';?>


  <!-- Page -->
  <div class="page">
 <!-- Panel Full Example -->
 <div class="page-content container-fluid">
 <div class="col-md-12">
          <!-- Panel Static Labels -->
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Static Labels</h3>
            </div>
            <div class="panel-body container-fluid">
              <form autocomplete="off">
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputText">Text</label>
                  <input type="text" class="form-control" id="inputText" name="inputText" placeholder="Text"
                  />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputEmail">Email</label>
                  <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email"
                  />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputPassword">Password</label>
                  <input type="password" class="form-control" id="inputPassword" name="inputPassword"
                    placeholder="Password" />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputFile">File</label>
                  <input type="text" class="form-control" placeholder="Browse.." readonly="" />
                  <input type="file" id="inputFile" name="inputFile" multiple="" />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="textarea">Textarea</label>
                  <textarea class="form-control" id="textarea" name="textarea" rows="3"></textarea>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="select">Select</label>
                  <select class="form-control" id="select">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="selectMulti">Multi Select</label>
                  <select class="form-control" id="selectMulti" multiple="">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputDisabled">Input Disabled</label>
                  <input type="text" class="form-control" id="inputDisabled" placeholder="Input Disabled"
                    disabled>
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputFocus">Input Focus</label>
                  <input type="text" class="form-control focus" id="inputFocus" placeholder="Input Focus">
                </div>
                <div class="form-group form-material has-warning" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputWarning">Input Warning</label>
                  <input type="text" class="form-control" id="inputWarning" placeholder="Input Warning">
                </div>
                <div class="form-group form-material has-danger" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputError">Input Danger</label>
                  <input type="text" class="form-control" id="inputError" placeholder="Input Error">
                </div>
                <div class="form-group form-material has-success" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputSuccess">Input Success</label>
                  <input type="text" class="form-control" id="inputSuccess" placeholder="Input Success">
                </div>
                <div class="form-group form-material has-info" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputInfo">Input Info</label>
                  <input type="text" class="form-control" id="inputInfo" placeholder="Input Info">
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputSmall">Small Input</label>
                  <input type="text" class="form-control form-control-sm" id="inputSmall" name="inputSmall"
                    placeholder="Small Input" />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputSmall">Default Input</label>
                  <input type="text" class="form-control" id="inputDefault" name="inputDefault" placeholder="Default Input"
                  />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputSmall">Large Input</label>
                  <input type="text" class="form-control form-control-lg" id="inputLarge" name="inputLarge"
                    placeholder="Large Input" />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputHint">Input Hint</label>
                  <input type="text" class="form-control" id="inputHint" name="inputHint" placeholder="Input Hint"
                    data-hint="Write here something cool" />
                </div>
                <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="form-control-label" for="inputAddons">Input addons</label>
                  <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <div class="form-control-wrap">
                      <input type="text" class="form-control" id="inputAddons" />
                    </div>
                    <span class="input-group-btn">
                      <button class="btn btn-info" type="button">Button</button>
                    </span>
                  </div>
                </div>
                <div class="form-group form-material row" data-plugin="formMaterial">
                  <div class="col-md-6">
                    <label class="form-control-label" for="inputGrid1">Input Grid</label>
                    <input type="text" class="form-control" id="inputGrid1" name="inputGrid1" placeholder="col-md-6"
                    />
                  </div>
                  <div class="col-md-6">
                    <label class="form-control-label" for="inputGrid2">Input Grid</label>
                    <input type="text" class="form-control" id="inputGrid2" name="inputGrid2" placeholder="col-md-6"
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- End Panel Static Labels -->
        </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <?php include 'includes/footer.php';?>
  <!-- Core  -->
  <script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="../global/vendor/babel-external-helpers/babel-external-helpersfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jquery/jquery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/popper-js/umd/popper.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/bootstrap/bootstrap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/animsition/animsition.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/mousewheel/jquery.mousewheel.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollbar/jquery-asScrollbar.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/asscrollable/jquery-asScrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/waves/waves.minfd53.js?v4.0.1"></script>

  <!-- Plugins -->
  <script src="../global/vendor/jquery-mmenu/jquery.mmenu.min.allfd53.js?v4.0.1"></script>
  <script src="../global/vendor/switchery/switchery.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/intro-js/intro.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/screenfull/screenfull.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/slidepanel/jquery-slidePanel.minfd53.js?v4.0.1"></script>

  <!-- Plugins For This Page -->
  <script src="../global/vendor/chartist/chartist.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/jquery-jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-enfd53.js?v4.0.1"></script>
  <script src="../global/vendor/matchheight/jquery.matchHeight-minfd53.js?v4.0.1"></script>
  <script src="../global/vendor/peity/jquery.peity.minfd53.js?v4.0.1"></script>

  <!-- Scripts -->
  <script src="../global/js/Component.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Base.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Config.minfd53.js?v4.0.1"></script>

  <script src="assets/js/Section/Menubar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/Sidebar.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/PageAside.minfd53.js?v4.0.1"></script>
  <script src="assets/js/Section/GridMenu.minfd53.js?v4.0.1"></script>
  <!-- Config -->
  <script src="../global/js/config/colors.minfd53.js?v4.0.1"></script>
  <script src="assets/js/config/tour.minfd53.js?v4.0.1"></script>
  <script>
    Config.set('assets', 'assets');
  </script>

  <!-- Page -->
  <script src="assets/js/Site.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/asscrollable.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/slidepanel.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/switchery.minfd53.js?v4.0.1"></script>

  <script src="../global/js/Plugin/matchheight.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/jvectormap.minfd53.js?v4.0.1"></script>
  <script src="../global/js/Plugin/peity.minfd53.js?v4.0.1"></script>


  <script src="assets/examples/js/dashboard/v1.minfd53.js?v4.0.1"></script>


</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/mmenu/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Jan 2020 07:34:21 GMT -->
</html>