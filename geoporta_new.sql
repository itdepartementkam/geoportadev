-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 22 jan. 2020 à 15:55
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `geoporta`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `phone1` varchar(255) NOT NULL,
  `phone2` varchar(255) NOT NULL,
  `id_languge` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_city` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`id`, `firstname`, `lastname`, `adress`, `phone1`, `phone2`, `id_languge`, `id_user`, `id_city`) VALUES
(1, 'Kary', 'Switland', '7384 Briar Crest Way', '948-910-3689', '331-302-7808', 2, 6, 3),
(2, 'Darcy', 'Pedden', '61340 Vidon Alley', '217-208-4441', '406-494-7764', 4, 5, 3),
(3, 'Nesta', 'Monard', '129 Forest Dale Center', '552-749-7855', '642-174-1970', 3, 1, 3),
(4, 'Ashby', 'Dike', '029 Vidon Road', '199-472-9972', '162-627-1419', 1, 4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `city`
--

INSERT INTO `city` (`id`, `name`, `zipcode`, `country_id`, `warehouse_id`, `create_time`) VALUES
(1, 'test', 123, 3, 13, '2020-01-07 23:00:00'),
(2, 'Hambourg1', 17, 3, 12, '2020-01-28 23:00:00'),
(3, 'Munich', 667, 3, 16, '2020-01-14 23:00:00'),
(4, 'Bavière', 586, 3, 15, '2020-01-21 23:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `barre_code` varchar(45) NOT NULL,
  `phone_code` varchar(45) NOT NULL,
  `create_time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `country`
--

INSERT INTO `country` (`id`, `name`, `barre_code`, `phone_code`, `create_time`) VALUES
(3, 'germany', '1468', '885', '2020-01-14 23:00:00'),
(4, 'Italy', '5873', '997', '2020-01-19 23:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone1` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `id_languge` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `customer`
--

INSERT INTO `customer` (`id`, `city_id`, `user_id`, `firstname`, `lastname`, `address`, `phone1`, `phone2`, `id_languge`, `create_time`) VALUES
(1, 4, 3, 'Berni', 'Nattriss', '51 Huxley Park', '002536789', '002987631', 2, '2020-01-26 23:00:00'),
(2, 3, 1, 'Cassey', 'Cayette', '50071 Meadow Valley Circle', '0078364515', '0029357641', 1, '2020-01-25 23:00:00'),
(3, 2, 4, 'Arnie', 'Keenleyside', '2 Hayes Lane', '739182567', '002547836', 3, '2020-01-19 23:00:00'),
(4, 3, 2, 'Feodor', 'Badsey', '864 Ridge Oak Terrace', '638754216', '966588715', 4, '2020-01-05 23:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `destination`
--

CREATE TABLE `destination` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `destination`
--

INSERT INTO `destination` (`id`, `city_id`, `firstname`, `lastname`, `address`, `id_user`) VALUES
(1, 1, 'Ulrica', 'Roger', '59 Eagan Circle', 1),
(2, 3, 'Hendrika', 'Fuller', '7 Oak Crossing', 2);

-- --------------------------------------------------------

--
-- Structure de la table `dimension`
--

CREATE TABLE `dimension` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `dimension`
--

INSERT INTO `dimension` (`id`, `name`, `height`, `width`, `length`) VALUES
(1, 'xlarge', 63, 10, 12),
(2, 'xsmall', 88, 11, 6);

-- --------------------------------------------------------

--
-- Structure de la table `drive`
--

CREATE TABLE `drive` (
  `id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `transporter_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `drive`
--

INSERT INTO `drive` (`id`, `vehicle_id`, `transporter_id`, `create_time`) VALUES
(1, 1, 2, '2020-01-14 23:00:00'),
(2, 2, 1, '2020-01-21 23:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `create_time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `language`
--

INSERT INTO `language` (`id`, `name`, `create_time`) VALUES
(1, 'French', '2020-01-01 23:00:00'),
(2, 'English', '2020-01-14 23:00:00'),
(3, 'Spanish', '2020-01-08 23:00:00'),
(4, 'almana', '2020-01-19 23:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `localisation`
--

CREATE TABLE `localisation` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `id_vehicle` int(11) DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL,
  `transporter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `localisation`
--

INSERT INTO `localisation` (`id`, `package_id`, `id_vehicle`, `warehouse_id`, `transporter_id`) VALUES
(1, 2, 2, 9, 2),
(2, 2, 1, 6, 2);

-- --------------------------------------------------------

--
-- Structure de la table `package`
--

CREATE TABLE `package` (
  `id` int(11) NOT NULL,
  `dimension_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `barre_code` varchar(45) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `fragile` tinyint(4) DEFAULT 0,
  `weight` int(11) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `package`
--

INSERT INTO `package` (`id`, `dimension_id`, `destination_id`, `customer_id`, `barre_code`, `name`, `description`, `fragile`, `weight`, `state`, `price`) VALUES
(1, 1, 1, 3, '7777', 'package 1', 'package 1', 0, 12, 'during', 250),
(2, 1, 1, 1, '9999', 'package 2', 'package 2', 0, 456, 'compled', 600),
(3, 2, 2, 2, '3333333', 'test', 'test', 0, 500, 'test', 400),
(4, 2, 2, 2, '951159', 'test2', 'test33333', 1, 900, 'test12', 1000),
(5, 1, 2, 2, '1234567899', 'chaima', 'testtest', 1, 200, 'test', 1245);

-- --------------------------------------------------------

--
-- Structure de la table `reclamation`
--

CREATE TABLE `reclamation` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reclamation`
--

INSERT INTO `reclamation` (`id`, `customer_id`, `package_id`, `commentaire`, `status`, `create_time`) VALUES
(1, 1, 2, 'what is the state of package1', 'compled', '2020-01-14 23:00:00'),
(2, 2, 1, 'what is the state of package2', 'during', '2020-01-09 10:29:49');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `his_role` varchar(45) NOT NULL,
  `create_time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `his_role`, `create_time`) VALUES
(1, 'admin', '2020-01-07 23:00:00'),
(2, 'customer', '2020-01-19 23:00:00'),
(3, 'manager', '2020-01-13 23:00:00'),
(4, 'transporter', '2020-01-06 23:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `transporter`
--

CREATE TABLE `transporter` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `salary` float NOT NULL,
  `phone1` varchar(255) NOT NULL,
  `phone2` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `transporter`
--

INSERT INTO `transporter` (`id`, `firstname`, `lastname`, `salary`, `phone1`, `phone2`, `user_id`, `create_time`) VALUES
(1, 'Johnette', 'Curson', 850, '495-872-3922', '309-962-5988', 5, '2020-01-06 23:00:00'),
(2, 'Cinda', 'Wanklyn', 900, '391-976-1672', '792-798-3622', 6, '2020-01-12 23:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `language_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `language_id`, `role_id`, `create_time`) VALUES
(1, 'Berni@gmail.com', 'Berni123', 2, 1, '2020-01-09 09:44:23'),
(2, 'Cassey@gmail.com', 'Cassey123', 1, 2, '2020-01-09 09:44:49'),
(3, 'Arnie@gmail.com', 'Arnie123', 4, 3, '2020-01-09 09:46:02'),
(4, 'Parnell@gmail.com', 'Parnell123', 1, 1, '2020-01-09 09:46:35'),
(5, 'Jesus@gmail.com', 'Jesus123', 3, 4, '2020-01-09 09:48:01'),
(6, 'Feodor@gmail.com', 'Feodor123', 2, 4, '2020-01-09 09:48:52');

-- --------------------------------------------------------

--
-- Structure de la table `vehicle`
--

CREATE TABLE `vehicle` (
  `id` int(11) NOT NULL,
  `number` varchar(45) NOT NULL,
  `brands` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `longitude` float NOT NULL,
  `latitude` decimal(2,0) NOT NULL,
  `fuel` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `wheight` float DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vehicle`
--

INSERT INTO `vehicle` (`id`, `number`, `brands`, `status`, `longitude`, `latitude`, `fuel`, `type`, `wheight`, `warehouse_id`) VALUES
(1, '1', 'Nissan', 'District of Columbia', 81, '78', 'gasoline', 'pickup', 500, 13),
(2, '2', 'Chevrolet', 'Florida', 5, '23', 'diesel', 'crossover', 400, 5);

-- --------------------------------------------------------

--
-- Structure de la table `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `id_warehousemanger` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `warehouse`
--

INSERT INTO `warehouse` (`id`, `name`, `address`, `id_warehousemanger`) VALUES
(1, 'Lucrèce', '67 8th Way', 1),
(2, 'Néhémie', '5 Paget Avenue', 2),
(3, 'Maéna', '185 Mcguire Hill', 4),
(4, 'Maëly', '114 Dapin Avenue', 3),
(5, 'Adèle', '37 Algoma Place', NULL),
(6, 'Torbjörn', '930 Coolidge Lane', NULL),
(7, 'Cécile', '21 Crowley Center', NULL),
(8, 'Géraldine', '614 Everett Junction', NULL),
(9, 'Daphnée', '00 Butterfield Center', NULL),
(10, 'chima', '85 tunis', NULL),
(11, 'taher', '60 pologne', NULL),
(12, 'Annotés', '2957 Lotheville Trail', NULL),
(13, 'Maïlis', '6 Hoard Plaza', NULL),
(14, 'Léana', '15074 Macpherson Drive', NULL),
(15, 'Bérangère', '7046 Raven Center', NULL),
(16, 'Josée', '6 Grim Pass', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `warehousemaneger`
--

CREATE TABLE `warehousemaneger` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phon1` varchar(255) NOT NULL,
  `phon2` varchar(255) NOT NULL,
  `id_languge` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_warehouse` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `warehousemaneger`
--

INSERT INTO `warehousemaneger` (`id`, `firstname`, `lastname`, `address`, `phon1`, `phon2`, `id_languge`, `id_city`, `id_user`, `id_warehouse`) VALUES
(1, 'Miof mela', 'Hadeke', '0 Saint Paul Trail', '428-550-1759', '847-571-2285', 4, 2, 2, 14),
(2, 'Corabel', 'Sprott', '70 Chive Point', '172-103-4895', '776-681-4494', 2, 3, 4, 16),
(3, 'Pincas', 'Wraight', '14 Bellgrove Road', '434-730-9355', '181-929-3479', 1, 4, 1, 9),
(4, 'Kelsey', 'Brierly', '85711 Cherokee Lane', '614-867-6527', '749-309-2575', 2, 2, 6, 13);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admin_language` (`id_languge`),
  ADD KEY `fk_admin_user` (`id_user`),
  ADD KEY `fk_admin_city` (`id_city`);

--
-- Index pour la table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_city_country1_idx` (`country_id`),
  ADD KEY `fk_city_warehouse1_idx` (`warehouse_id`);

--
-- Index pour la table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD UNIQUE KEY `barre_code_UNIQUE` (`barre_code`),
  ADD UNIQUE KEY `phone_code_UNIQUE` (`phone_code`);

--
-- Index pour la table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_customer_city1_idx` (`city_id`),
  ADD KEY `fk_customer_user1_idx` (`user_id`),
  ADD KEY `fk_customer_languge` (`id_languge`);

--
-- Index pour la table `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_destination_city1_idx` (`city_id`),
  ADD KEY `fk_destination_user` (`id_user`);

--
-- Index pour la table `dimension`
--
ALTER TABLE `dimension`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Index pour la table `drive`
--
ALTER TABLE `drive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_drive_vehicle1_idx` (`vehicle_id`),
  ADD KEY `fk_drive_transporter1_idx` (`transporter_id`);

--
-- Index pour la table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Index pour la table `localisation`
--
ALTER TABLE `localisation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_localisation_package1_idx` (`package_id`),
  ADD KEY `fk_localisation_warehouse1_idx` (`warehouse_id`),
  ADD KEY `fk_localisation_transporter1_idx` (`transporter_id`),
  ADD KEY `fk_localisation_vehicle` (`id_vehicle`);

--
-- Index pour la table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `barre_code_UNIQUE` (`barre_code`),
  ADD KEY `fk_package_dimension1_idx` (`dimension_id`),
  ADD KEY `fk_package_destination1_idx` (`destination_id`),
  ADD KEY `fk_package_customer1_idx` (`customer_id`);

--
-- Index pour la table `reclamation`
--
ALTER TABLE `reclamation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reclamation_customer1_idx` (`customer_id`),
  ADD KEY `fk_reclamation_package1_idx` (`package_id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `his_role_UNIQUE` (`his_role`);

--
-- Index pour la table `transporter`
--
ALTER TABLE `transporter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transporter_user1_idx` (`user_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_user_language_idx` (`language_id`),
  ADD KEY `fk_user_role1_idx` (`role_id`);

--
-- Index pour la table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `number_UNIQUE` (`number`),
  ADD KEY `fk_vehicle_warehouse1_idx` (`warehouse_id`);

--
-- Index pour la table `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_warehousemanger` (`id_warehousemanger`);

--
-- Index pour la table `warehousemaneger`
--
ALTER TABLE `warehousemaneger`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `fk_warehousemaneger_city` (`id_city`),
  ADD KEY `fk_warehousemaneger_languge` (`id_languge`),
  ADD KEY `fk_warehousemaneger_user` (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `destination`
--
ALTER TABLE `destination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `dimension`
--
ALTER TABLE `dimension`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `drive`
--
ALTER TABLE `drive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `localisation`
--
ALTER TABLE `localisation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `package`
--
ALTER TABLE `package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `reclamation`
--
ALTER TABLE `reclamation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `transporter`
--
ALTER TABLE `transporter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `warehousemaneger`
--
ALTER TABLE `warehousemaneger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_admin_city` FOREIGN KEY (`id_city`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_admin_language` FOREIGN KEY (`id_languge`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_admin_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `fk_city_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_city_warehouse1` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `fk_customer_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_customer_languge` FOREIGN KEY (`id_languge`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_customer_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `destination`
--
ALTER TABLE `destination`
  ADD CONSTRAINT `fk_destination_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_destination_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `drive`
--
ALTER TABLE `drive`
  ADD CONSTRAINT `fk_drive_transporter1` FOREIGN KEY (`transporter_id`) REFERENCES `transporter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_drive_vehicle1` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `localisation`
--
ALTER TABLE `localisation`
  ADD CONSTRAINT `fk_localisation_package1` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_localisation_transporter1` FOREIGN KEY (`transporter_id`) REFERENCES `transporter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_localisation_vehicle` FOREIGN KEY (`id_vehicle`) REFERENCES `vehicle` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_localisation_warehouse1` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `package`
--
ALTER TABLE `package`
  ADD CONSTRAINT `fk_package_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_package_destination1` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_package_dimension1` FOREIGN KEY (`dimension_id`) REFERENCES `dimension` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `reclamation`
--
ALTER TABLE `reclamation`
  ADD CONSTRAINT `fk_reclamation_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reclamation_package1` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `transporter`
--
ALTER TABLE `transporter`
  ADD CONSTRAINT `fk_transporter_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_language` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `vehicle`
--
ALTER TABLE `vehicle`
  ADD CONSTRAINT `fk_vehicle_warehouse1` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `warehouse`
--
ALTER TABLE `warehouse`
  ADD CONSTRAINT `fk_warehouse_warehousemaneger` FOREIGN KEY (`id_warehousemanger`) REFERENCES `warehousemaneger` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `warehousemaneger`
--
ALTER TABLE `warehousemaneger`
  ADD CONSTRAINT `fk_warehousemaneger_city` FOREIGN KEY (`id_city`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_warehousemaneger_languge` FOREIGN KEY (`id_languge`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_warehousemaneger_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_warehousemaneger_warehouse` FOREIGN KEY (`id_warehouse`) REFERENCES `warehouse` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
